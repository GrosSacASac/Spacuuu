/* there is no way to know if a device truly supports vibration before using it
https://github.com/Modernizr/Modernizr/issues/1217
https://developer.mozilla.org/en-US/docs/Web/API/Navigator/vibrate
*/

export {vibrate, vibrateForPermission, vibrationSupport};
import * as d from "../node_modules/dom99/built/dom99ES.js"; 


let vibrate;
let vibrateForPermission;

const vibrationSupport = (
    window.navigator &&
    window.navigator.vibrate
);

if (vibrationSupport) {
    let vibratedAtLeastOnce = false;
    vibrate = function (pattern) {
        navigator.vibrate(pattern);
    };
    vibrateForPermission = function (pattern) {
        /* returns false if it did not vibrate this time */
        if (vibratedAtLeastOnce) {
            return false;
        }
        try {
            vibrate(50); 
            vibratedAtLeastOnce = true;
            return true;
        } catch (error) {
            return false;
        }
    };
} else {
    vibrate = function () {};
    vibrateForPermission = vibrate;
}

