/* usage
import {gamePadSupport, getGamePadInput, lastGamePadInput} from "gamepads.js";
function loop() {
    getGamePadInput();
    updateGameState(lastGamePadInput);
    updateDisplay();
    requestAnimationFrame(loop);
}
// to get a mapping of a gamepad https://luser.github.io/gamepadtest/
todo also import and use onAllGamePadDisconnected
todo split MIN_AXES_VALUE into MIN_AXES_VALUE for game (lower) and MIN_AXES_VALUE
for menus (higher)
*/
export {gamePadMap} from "./settings/gamePadMap.js";
export {gamePadSupport, getGamePadInput, lastGamePadInput};
import {onWantToUseGamePad} from "./main.js";

const gamePadSupport = (
    /*"Gamepad" in window &&*/
    "GamepadEvent" in window &&
    window.navigator &&
    window.navigator.getGamepads
);

const emptyFunction = function () {};
let getGamePadInput = emptyFunction;

const SETTINGS = {
    /* Must be in range 0.0 to 1.0
    The closer to value to 0, the better it is for precision
    The closer to value to 1, the better it is for GamePads
    that are slightly de-calibrated or old */
    MIN_AXES_VALUE : 0.215,
    MAX_CONTROLLERS: 16
};

const lastGamePadInput = {
    anyControllerEverPluggedIn: false,
    controllers: Array(SETTINGS.MAX_CONTROLLERS)
};

const controllers = {}; // todo make this more like lastGamePadInput.controllers

(function () {
    if (!gamePadSupport) {
        return;
    }
    
    window.addEventListener("gamepadconnected", function connecthandler(event) {
        onWantToUseGamePad();
        const gamepad = event.gamepad;
        controllers[gamepad.index] = gamepad;
        if (!lastGamePadInput.anyControllerEverPluggedIn) {
            lastGamePadInput.anyControllerEverPluggedIn = true;
            

            const scanGamepadsChrome = function () {
                // required for chrome
                // in chrome the gamepad object is only a snapshot (frozen)
                // to get the last value we have to navigator.getGamepads()
                // in ff this should have no observable effect
                const gamepads = navigator.getGamepads();
                for (let i = 0; i < gamepads.length; i += 1) {
                    if (gamepads[i]) {
                        controllers[gamepads[i].index] = gamepads[i];
                    }
                }
            };

            getGamePadInput = function () {
                // updates lastGamePadInput, with normalized input
                scanGamepadsChrome();
                Object.values(controllers).forEach(function (controller) {
                    const axes = [];  
                    for (let i = 0; i < controller.axes.length; i += 1) {
                        let value = controller.axes[i]; /* -1.0 to +1.0 */
                        if (Math.abs(value) < SETTINGS.MIN_AXES_VALUE) {
                            value = 0;
                        }
                        axes.push(value);
                    }
                    lastGamePadInput.controllers[controller.index] = {
                        buttons: controller.buttons,
                        axes
                    };
                });
            };
        }
    });

    window.addEventListener("gamepaddisconnected", function (event) {
        const gamepad = event.gamepad
        delete controllers[gamepad.index];
    });

}());
