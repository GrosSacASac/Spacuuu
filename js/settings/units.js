import * as d from "../../node_modules/dom99/built/dom99ES.js";
//import {vibrationSupport, vibrate} from "./vibration.js";
//import {deathPattern, hurtPattern,bossPattern} from "./settings/vibrationPatterns.js";
//import {playSound} from "./sound.js";
import {setXPosition, setYPosition, setRadianAngle } from "../dom.js";
import {stopUseSpawn, clampValue,
rotatingValue, angleFromVector, insideAxis} from "../2DMath.js";
import {enemyPool, enemyBombPool, alliedBombPool} from "../pools.js";
import {scoreKill} from "../scores.js";
import {yMultiplier, xMultiplier, spawnGrids,
tickCount, height, width, l, gameState} from "../main.js";
import {softExtend} from "../abstract/classes.js";
import {isNewPositionInConflictEnemyPool} from "../bodies.js";
// could combine  yMultiplier * and player.slowMultiplier
export {Ships, unitsWithBaseSpeed, player, Player, Players};

const Ships = {};

const addShip = function (Ship) {
    Ships[Ship.id] = Ship;
    return Ship;
};

const addShootCenter = function (Ship) {
    Ship.shootCenter = (Ship.width / 2) - (Ship.weapon.width / 2);
    return Ship;
};


const PlayerBomb = {
    id: "PlayerBomb",
    width: 5,
    height: 20,
    maxAge: 1200,
    baseSpeedX: 0,
    speedX: 0,
    baseSpeedY: 5,
    speedY: 5,
    bombInitialY: undefined,
    descriptor: {
       tagName: "div",
       class: "bomb"
    },
    create: function (x) {
        /* should only set application -time constants*/
        const instance = {
            is: PlayerBomb
        };
        instance.dom = d.createElement2(instance.is.descriptor);
        instance.is.configure(instance, x);
        return instance;
    },
    configure: function (instance, x) {
        /*should set the same things as create
        receives as arguments*/
        instance.life = instance.is.life;
        instance.birthDate = tickCount;
        instance.x = x;
        instance.y = instance.is.bombInitialY;
    },
    cleanUp: function (instance) {

    }
    /*beforeDeActivated: function (instance) {

    }*/
};

const Player = addShootCenter({
    width: 25,
    height: 25,
    shootCenter: 0,
    weapon: PlayerBomb,
    reset: function (instance) {
      Object.assign(instance, Player.defaultStart);
      instance.shielderY = Math.round(height / 2),
      instance.shielderYCenter = instance.shielderY + instance.shielderYHalfHeight,
      instance.y = height - instance.height;
      instance.maxX = width - instance.width;
    },
    assignForSave: function (target, aPlayer) {
      // only saves non constants
      Player.defaultStartKeys.forEach(function (key) {
        target[key] = aPlayer[key];
      });
    },
    playerShoot: function (instance) {
        if (instance.nextTimeShoot === 0) {
            alliedBombPool.activate(instance.weapon, instance.x + instance.shootCenter);
            instance.nextTimeShoot = instance.shootInterval;
        } else {
          instance.nextTimeShoot += -1;
        }
    },
    // not constant
    defaultStart: {
        nextTimeShoot: 30,
        life: 3,
        shields: 3,
        shieldActive: false,
        shieldTimeRemaining: 0,
        slowTime: 3,
        swipes: 3,
        nukeActive: false,
        nukeEndFrame: 0,
        nukes: 2,
        shielders: 4,
        shielderX: 0,
        shielderXCenter: 7,
        shielderYCenter: 7,
        x: 10,
        y: 5,
        maxX: 11,
        turn: 1
    },

    create: function () {
      const player = Object.assign({
        is: Player,
        id: "player",
        width: Player.width,
        height: Player.height,
        shootCenter: Player.shootCenter,
        weapon: Player.weapon,
        baseSpeedX: 2,
        speedX: 2,
        baseSpeedY: 0,
        speedY: 0,
        shield: {},
        shootInterval: 30,
        shieldOverlap: 60, /* 60 frames before the shield end you can already cast again */
        shieldDuration: 300,
        survivalShieldDuration: 60,
        slowTimeDuration: 800,
        swipeDuration: 25,
        shielderDuration: 1500,
        shielderDelay: 25,
        shielderInvertedDelay: 1475,
        nukeDuration: 84,
        nukeDelay: 30,
        nukeInvertedDelay: 54,
        // see offset in css .shielderlaser
        shielderXHalfWidth: 7,
        shielderYHalfHeight: 7,
        // not constant
        }, Player.defaultStart);
      return player;
    }
});
Player.defaultStartKeys = Object.keys(Player.defaultStart);

const player = Player.create();
const Players = [player];

const BaseBomb = {
    configure: function (instance, x, y) {
        /*
        called when created or reactivated
        sets unit-time constants and initial values
        should set x, y values
        */
        instance.x = x;
        instance.y = y;
        instance.birthDate = tickCount;
        instance.life = instance.is.life;
    },
    cleanUp: function (instance) {
        /*
        reset things from previous use
        but not required in the first create()
        */
        instance.dom.classList.remove("targeted");
    }
};

const Bomb1 = softExtend(BaseBomb, {
    id: "Bomb1",
    width: 5,
    height: 18,
    maxAge: 1200,
    life: 18,
    baseSpeedY: 2,
    speedY: 2,
    descriptor: {
       tagName: "div",
       class: "bomb1"
    },
    create: function (x, y) {
        const instance = {
            is: Bomb1
        };
        instance.dom = d.createElement2(instance.is.descriptor);
        instance.is.configure(instance, x, y);
        return instance;
    },
});

const DiagonaleBomb = softExtend(BaseBomb, {
    id: "DiagonaleBomb",
    width: 5,
    height: 18,
    maxAge: 1200,
    life: 18,
    baseSpeedX: 0.3,
    speedX: 0.3,
    baseSpeedY: 0.9,
    speedY: 0.9,
    randomTickPeriod: 60,
    descriptor: {
       tagName: "div",
       class: "diagonale-bomb"
    },
    configure: function (instance, x, y) {
        BaseBomb.configure(instance, x, y);
        instance.nextRandomTick = instance.is.randomTickPeriod;
        instance.direction = 0;
    },
    create: function (x, y) {
        const instance = {
            is: DiagonaleBomb
        };
        instance.dom = d.createElement2(instance.is.descriptor);
        instance.is.configure(instance, x, y);
        return instance;
    },
    move: function (instance) {
       if (instance.nextRandomTick > 0) {
          instance.nextRandomTick -= 1;
       } else {
          instance.nextRandomTick = instance.is.randomTickPeriod;
         const random = Math.random();;
         instance.direction = 0;
         if (random >= 0.5) {
            instance.direction = 1;
         } else {
            instance.direction = -1;
         }
       }
       
       instance.y += instance.is.speedY / gameState.slowedDivider;
       instance.x += (instance.direction * instance.is.speedX) / gameState.slowedDivider;
    },
});

const MultiDirectionalBomb = softExtend(BaseBomb, {
    id: "MultiDirectionalBomb",
    width: 5,
    height: 18,
    maxAge: 1200,
    life: 18,
    descriptor: {
       tagName: "div",
       class: "diagonale-bomb"
    },
    create: function (x, y, speedX, speedY) {
        const instance = {
            is: MultiDirectionalBomb
        };
        instance.dom = d.createElement2(instance.is.descriptor);
        instance.is.configure(instance, x, y, speedX, speedY);
        return instance;
    },
    configure: function (instance, x, y, speedX, speedY) {
        BaseBomb.configure(instance, x, y);
        instance.speedX = speedX;
        instance.speedY = speedY;
        // could remove offset after image has 0 angle
        const angleRadian = angleFromVector(speedX, speedY, /*offset=*/-Math.PI / 2);
        setRadianAngle(instance.dom, angleRadian);
    },
    move: function (instance) {
       instance.y += instance.speedY / gameState.slowedDivider;
       instance.x += instance.speedX / gameState.slowedDivider;
    }
});

const DownFacingBomb = softExtend(BaseBomb, {
    id: "DownFacingBomb",
    width: 5,
    height: 18,
    maxAge: 1200,
    life: 18,
    stepNormalizeDirection: 0.0035,
    descriptor: {
       tagName: "div",
       class: "diagonale-bomb"
    },
    create: function (x, y, speedX, speedY) {
        const instance = {
            is: DownFacingBomb
        };
        instance.dom = d.createElement2(instance.is.descriptor);
        instance.is.configure(instance, x, y, speedX, speedY);
        return instance;
    },
    configure: function (instance, x, y, speedX, speedY) {
        BaseBomb.configure(instance, x, y);
        instance.speedX = speedX;
        instance.speedY = speedY;
    },
    move: function (instance) {
        /* decelerates x axis each round and accelerates y axis by same amount
        */
       instance.y += instance.speedY / gameState.slowedDivider;
       instance.x += instance.speedX / gameState.slowedDivider;
       if (Math.abs(instance.speedX) > 0) {

        let sign = 1;
        if (instance.speedX < 0) {
            sign = -1;
        }
        instance.speedX = (Math.abs(instance.speedX) - instance.is.stepNormalizeDirection) * sign;
        instance.speedY += instance.is.stepNormalizeDirection;
       }
    }
});

const DroneMissile = softExtend(BaseBomb, {
    id: "DroneMissile",
    width: 16,
    height: 16,
    maxAge: 1200,
    life: 12,
    baseSpeedX: 0.1,
    speedX: 0.1,
    baseSpeedY: 0.2,
    speedY: 0.2,
    accelerationX: 0.0001,
    accelerationY: 0.006,
    descriptor: {
       tagName: "div",
       class: "drone-missile"
    },
    create: function (x, y) {
    /* should only set application -time constants*/
        const instance = {
            is: DroneMissile
        };
        instance.dom = d.createElement2(instance.is.descriptor);
        instance.is.configure(instance, x, y);
        return instance;
    },
    configure: function (instance, x, y) {
        BaseBomb.configure(instance, x, y);
        instance.speedX = instance.is.speedX;
        instance.speedY = instance.is.speedY;
    },
    move: function (instance) {
       if (instance.x > player.x) {
          instance.x -= instance.speedX / gameState.slowedDivider;
       } else if (instance.x < player.x) {
          instance.x += instance.speedX / gameState.slowedDivider;
       }
       instance.y += instance.speedY / gameState.slowedDivider;

       // todo make interaction with slow time accurate
       // accelerates
       instance.speedX += xMultiplier * instance.is.accelerationX / gameState.slowedDivider;
       instance.speedY += yMultiplier * instance.is.accelerationY / gameState.slowedDivider;
    }
});

const ExplosionCapsule = softExtend(BaseBomb, {
    id: "ExplosionCapsule",
    width: 16,
    height: 16,
    maxAge: 1200,
    life: 12,
    baseSpeedX: 2,
    speedX: 2,
    baseSpeedY: 2,
    speedY: 2,
    descriptor: {
       tagName: "div",
       class: "explosion-capsule"
    },
    create: function (x, y) {
    /* should only set application -time constants*/
        const instance = {
            is: ExplosionCapsule
        };
        instance.dom = d.createElement2(instance.is.descriptor);
        instance.is.configure(instance, x, y);
        return instance;
    },
    configure: function (instance, x, y) {
        BaseBomb.configure(instance, x, y);
        instance.speedX = instance.is.speedX;
        instance.speedY = instance.is.speedY;
    },
    move: function (instance) {
       if (instance.y > (height / 2)) {
          
          const baseSpeedX = ExplosionDebris.speedX;
          const baseSpeedY = ExplosionDebris.speedY;
          enemyBombPool.deActivate(instance);
          enemyBombPool.activate(
                ExplosionDebris,
                instance.x,
                instance.y,
                baseSpeedX,
                baseSpeedY
          );
          enemyBombPool.activate(
                ExplosionDebris,
                instance.x,
                instance.y,
                -baseSpeedX,
                -baseSpeedY
          );
          enemyBombPool.activate(
                ExplosionDebris,
                instance.x,
                instance.y,
                -baseSpeedX,
                baseSpeedY
          );
          enemyBombPool.activate(
                ExplosionDebris,
                instance.x,
                instance.y,
                baseSpeedX,
                -baseSpeedY
          );
       } else {
          instance.y += instance.speedY / gameState.slowedDivider;
       }
       
       
    }
});


const ExplosionDebris = softExtend(BaseBomb, {
    id: "ExplosionDebris",
    width: 16,
    height: 16,
    maxAge: 1000,
    life: 2,
    baseSpeedX: 1.5,
    speedX: 1.5,
    baseSpeedY: 1.5,
    speedY: 1.5,
    descriptor: {
       tagName: "div",
       class: "explosion-debris"
    },
    create: function (x, y, speedX, speedY) {
    /* should only set application -time constants*/
        const instance = {
            is: ExplosionDebris
        };
        instance.dom = d.createElement2(instance.is.descriptor);
        instance.is.configure(instance, x, y, speedX, speedY);
        return instance;
    },
    configure: function (instance, x, y, speedX, speedY) {
        BaseBomb.configure(instance, x, y);
        instance.speedX = speedX;
        instance.speedY = speedY;
    },
    move: function (instance) {
       instance.x += instance.speedX / gameState.slowedDivider;
       instance.y += instance.speedY / gameState.slowedDivider;

    }
});
const WallBomb = softExtend(BaseBomb, {
    id: "WallBomb",
    width: Infinity,
    height: 18,
    maxAge: 1200,
    life: 40,
    baseSpeedY: 2,
    speedY: 2,
    descriptor: {
       tagName: "div",
       class: "wallbomb"
    },
    create: function (y) {
    /* should only set application -time constants*/
        const instance = {
            is: WallBomb,
            x: 0
        };
        instance.dom = d.createElement2(instance.is.descriptor);
        instance.is.configure(instance, y);
        return instance;
    },
    configure: function (instance, y) {
        /*should set the same things as create
        receives as arguments*/
        instance.life = instance.is.life;
        instance.birthDate = tickCount;
        instance.y = y;
        return instance;
    }
});


///////// ---- Ships ----- //////////


const BaseShip = {
    name: "BaseShip",
    shootCenter: 0,
    usesSpawn: true,
    points: 150,
    configure: function (instance, x, y) {
        /*
        called when created or reactivated
        sets unit-time constants and initial values
        should set x, y values
        */
        instance.x = x;
        instance.y = y;
        instance.birthDate = tickCount;
        instance.life = instance.is.life;
        instance.nextTimeShoot = instance.is.shootInterval;
    },
    shoot: function (instance) {
        if (instance.nextTimeShoot === 0) {
            enemyBombPool.activate(
                instance.is.weapon,
                instance.x + instance.is.shootCenter,
                instance.y + instance.is.height
            );
            instance.nextTimeShoot = instance.is.shootInterval;
        } else {
            instance.nextTimeShoot += -1;
        }
    },
    cleanUp: function (instance) {
        /*
        reset things from previous use
        but not required in the first create()
        */
        instance.dom.classList.remove("hurt");
    },
    beforeDeActivated: function (instance) {
        /*
        before the unit is destroyed
        */
        if (instance.is.usesSpawn) {
            stopUseSpawn(spawnGrids[instance.is.width], instance.x);
        }
        scoreKill(instance.is.points);
    }
};

const EnemySimple = addShip(softExtend(BaseShip, addShootCenter({
    name: "EnemySimple",
    id: "EnemySimple",
    points: 100,
    width: 25,
    height: 25,
    usesSpawn: true,
    weapon: Bomb1,
    life: 1,
    shootCenter: 0,
    shootInterval: 300,
    descriptor: {
       tagName: "div",
       "class": "enemy-simple"
    },
    create: function (x, y) {
        const instance = {
            is: EnemySimple
        };
        instance.dom = d.createElement2(instance.is.descriptor);
        instance.is.configure(instance, x, y);
        return instance;
    }
})));

const EnemyDiagonale = addShip(softExtend(BaseShip, addShootCenter({
    name: "EnemyDiagonale",
    id: "EnemyDiagonale",
    points: 150,
    width: 25,
    height: 25,
    usesSpawn: true,
    weapon: DiagonaleBomb,
    shootCenter: 0,
    life: 2,
    shootInterval: 70,
    descriptor: {
       tagName: "div",
       "class": "enemy-diagonale"
    },
    create: function (x, y) {
        const instance = {
            is: EnemyDiagonale
        };
        instance.dom = d.createElement2(instance.is.descriptor);
        instance.is.configure(instance, x, y);
        return instance;
    },
    beforeDeActivated: function (instance) {
        stopUseSpawn(spawnGrids[instance.is.width], instance.x);
        scoreKill(instance.is.points);
    }
})));

const Rotator = addShip(softExtend(BaseShip, addShootCenter({
    name: "Rotator",
    id: "Rotator",
    points: 230,
    width: 20,
    height: 30,
    baseSpeedX: 0.4,
    speedX: 0.4,
    baseSpeedY: 1,
    speedY: 1,
    usesSpawn: false,
    shootCenter: 0,
    weapon: DiagonaleBomb,
    life: 1,
    shootInterval: 80,
    changeMoveDirectionInterval: 100,
    descriptor: {
       tagName: "div",
       "class": "enemy-rotator"
    },
    configure: function (instance, x, y) {
        BaseShip.configure(instance, x, y);
        instance.speedX = instance.is.speedX;
        instance.moveDirection = 1;
        instance.ticksBeforeChangeMoveDirection = instance.is.changeMoveDirectionInterval;
    },
    create: function (x, y) {
        const instance = {
            is: Rotator
        };
        instance.dom = d.createElement2(instance.is.descriptor);
        instance.is.configure(instance, x, y);
        return instance;
    },
    getPotentialX: function (instance) {
        const distanceX = instance.moveDirection * instance.speedX;
        const maybeNewXPostion = clampValue(
            instance.x + distanceX,
            /*min=*/0,
            /*max=*/width - instance.is.width
        );

        if (isNewPositionInConflictEnemyPool(instance.is.width, instance.is.height, maybeNewXPostion, instance.y, instance)) {
            return instance.x;
        }
        return maybeNewXPostion;
    },
    move: function (instance) {
        const newPositionX = instance.is.getPotentialX(instance);
        if (newPositionX !== instance.x) {
            instance.x = newPositionX;
            setXPosition(instance.dom, instance.x);
        }

        instance.ticksBeforeChangeMoveDirection -= 1;
        if (instance.ticksBeforeChangeMoveDirection === 0) {
            instance.ticksBeforeChangeMoveDirection = instance.is.changeMoveDirectionInterval;
            instance.moveDirection *= -1;
        }
    }
})));

const ExplosionLauncher = addShip(softExtend(BaseShip, addShootCenter({
    name: "ExplosionLauncher",
    id: "ExplosionLauncher",
    points: 800,
    width: 40,
    height: 40,
    baseSpeedX: 0.7,
    speedX: 0.7,
    baseSpeedY: 1,
    speedY: 1,
    usesSpawn: false,
    shootCenter: 0,
    weapon: ExplosionCapsule,
    life: 2,
    shootInterval: 260,
    changeMoveDirectionInterval: 125,
    descriptor: {
       tagName: "div",
       "class": "explosion-launcher"
    },
    configure: function (instance, x, y) {
        BaseShip.configure(instance, x, y);
        instance.speedX = instance.is.speedX;
        instance.moveDirection = 1;
        instance.ticksBeforeChangeMoveDirection = instance.is.changeMoveDirectionInterval;
    },
    create: function (x, y) {
        const instance = {
            is: ExplosionLauncher
        };
        instance.dom = d.createElement2(instance.is.descriptor);
        instance.is.configure(instance, x, y);
        return instance;
    },
    getPotentialX: function (instance) {
        const distanceX = instance.moveDirection * instance.speedX;
        const maybeNewXPostion = clampValue(
            instance.x + distanceX,
            /*min=*/0,
            /*max=*/width - instance.is.width
        );

        if (isNewPositionInConflictEnemyPool(instance.is.width, instance.is.height, maybeNewXPostion, instance.y, instance)) {
            return instance.x;
        }
        return maybeNewXPostion;
    },
    move: function (instance) {
        const newPositionX = instance.is.getPotentialX(instance);
        if (newPositionX !== instance.x) {
            instance.x = newPositionX;
            setXPosition(instance.dom, instance.x);
        }

        instance.ticksBeforeChangeMoveDirection -= 1;
        if (instance.ticksBeforeChangeMoveDirection === 0) {
            instance.ticksBeforeChangeMoveDirection = instance.is.changeMoveDirectionInterval;
            instance.moveDirection *= -1;
        }
    }
})));

const BossTargeter = addShip(softExtend(BaseShip, {
    name: "BossTargeter",
    id: "BossTargeter",
    points: 1000,
    width: 40,
    height: 40,
    usesSpawn: true,
    weapon: DroneMissile,
    life: 2,
    shootInterval: 40,
    descriptor: {
       tagName: "div",
       "class": "boss-targeter"
    },
    create: function (x, y) {
        const instance = {
            is: BossTargeter
        };
        instance.dom = d.createElement2(instance.is.descriptor);
        instance.is.configure(instance, x, y);
        return instance;
    },
    configure: function (instance, x, y) {
        BaseShip.configure(instance, x, y);
        instance.nextShotAtLeft = true;
    },
    shoot: function (instance) {
        if (instance.nextTimeShoot === 0) {
            let x;
            if (instance.nextShotAtLeft) {
                x = Math.max(0, instance.x - 50);
            } else {
                x = Math.min(player.maxX, instance.x + 50);

            }
            enemyBombPool.activate(instance.is.weapon, x,  instance.y);
            instance.nextTimeShoot = instance.is.shootInterval;
            instance.nextShotAtLeft = !instance.nextShotAtLeft;
        } else {
            instance.nextTimeShoot += -1;
        }
    }
}));

const BossSwiper = addShip(softExtend(BaseShip, addShootCenter({
    name: "BossSwiper",
    id: "BossSwiper",
    points: 800,
    width: 40,
    height: 40,
    usesSpawn: true,
    weapon: MultiDirectionalBomb,
    life: 3,
    shootCenter: 0,
    shootInterval: 35,
    weaponBaseSpeed: 0.75,
    weaponBaseSpeedYAdder: 0.5,
    descriptor: {
       tagName: "div",
       "class": "boss-targeter"
    },
    create: function (x, y) {
        const instance = {
            is: BossSwiper
        };
        instance.dom = d.createElement2(instance.is.descriptor);
        instance.is.configure(instance, x, y);
        return instance;
    },
    configure: function (instance, x, y) {
        BaseShip.configure(instance, x, y);
        instance.incrementer = 0;
        instance.weaponSpeedYAdder =  instance.is.weaponBaseSpeedYAdder * yMultiplier;
        instance.weaponSpeed =  instance.is.weaponBaseSpeed * yMultiplier;
    },
    shoot: function (instance) {
        if (instance.nextTimeShoot === 0) {
            enemyBombPool.activate(
                instance.is.weapon,
                instance.x + instance.is.shootCenter,
                instance.y + instance.is.height,
                Math.cos(instance.incrementer * 0.0625 * Math.PI) * instance.weaponSpeed,
                Math.abs(
                    Math.sin(instance.incrementer * 0.0625 * Math.PI) * instance.weaponSpeed
                ) + instance.weaponSpeedYAdder
            );
            instance.nextTimeShoot = instance.is.shootInterval;
            instance.incrementer += 1;
        } else {
            instance.nextTimeShoot += -1;
        }
    }
})));

const ConfusingShooter = addShip(softExtend(BaseShip, addShootCenter({
    name: "ConfusingShooter",
    id: "ConfusingShooter",
    points: 800,
    width: 40,
    height: 40,
    usesSpawn: true,
    weapon: DownFacingBomb,
    life: 1,
    shootCenter: 0,
    shootInterval: 120,
    weaponBaseSpeed: 0.68,
    weaponBaseSpeedYAdder: 0.22,
    descriptor: {
       tagName: "div",
       "class": "boss-targeter"
    },
    create: function (x, y) {
        const instance = {
            is: ConfusingShooter
        };
        instance.dom = d.createElement2(instance.is.descriptor);
        instance.is.configure(instance, x, y);
        return instance;
    },
    configure: function (instance, x, y) {
        BaseShip.configure(instance, x, y);
        instance.incrementer = 0;
        instance.weaponSpeedYAdder =  instance.is.weaponBaseSpeedYAdder * yMultiplier;
        instance.weaponSpeed =  instance.is.weaponBaseSpeed * yMultiplier;
    },
    shoot: function (instance) {
        if (instance.nextTimeShoot === 0) {
            const speedX = rotatingValue(instance.incrementer);
            enemyBombPool.activate(
                instance.is.weapon,
                instance.x + instance.is.shootCenter,
                instance.y + instance.is.height,
                speedX * instance.weaponSpeed,
                (1 - Math.abs(speedX)) * instance.weaponSpeed + instance.weaponSpeedYAdder
            );
            instance.nextTimeShoot = instance.is.shootInterval;
            instance.incrementer += 1;
        } else {
            instance.nextTimeShoot += -1;
        }
    }
})));

const Dodger = addShip(softExtend(BaseShip, addShootCenter({
    name: "Dodger",
    id: "Dodger",
    points: 800,
    width: 40,
    height: 40,
    usesSpawn: false,
    weapon: DownFacingBomb,
    life: 1,
    shootCenter: 0,
    shootInterval: 100,
    weaponBaseSpeed: 0.68,
    weaponBaseSpeedYAdder: 0.22,
    delayInBetweenMoves: 90,
    moveDuration: 30,
    baseSpeedX: 2,
    speedX: 2,
    baseSpeedY: 0,
    speedY: 0,
    descriptor: {
       tagName: "div",
       "class": "boss-targeter"
    },
    create: function (x, y) {
        const instance = {
            is: Dodger
        };
        instance.dom = d.createElement2(instance.is.descriptor);
        instance.is.configure(instance, x, y);
        return instance;
    },
    configure: function (instance, x, y) {
        BaseShip.configure(instance, x, y);
        instance.nextMoveDirection = 1;
        instance.nextTimeMove = instance.is.delayInBetweenMoves;
        instance.nextTimePause = instance.is.moveDuration;
        instance.incrementer = 0;
        instance.weaponSpeedYAdder =  instance.is.weaponBaseSpeedYAdder * yMultiplier;
        instance.weaponSpeed =  instance.is.weaponBaseSpeed * yMultiplier;
    },
    getPotentialX: function (instance) {
        const maybeNewXPostion = clampValue(
            instance.x + instance.is.speedX * instance.nextMoveDirection,
            /*min=*/0,
            /*max=*/width - instance.is.width
        );

        if (isNewPositionInConflictEnemyPool(instance.is.width, instance.is.height, maybeNewXPostion, instance.y, instance)) {
            return instance.x;
        }
        return maybeNewXPostion;
    },
    move: function (instance) {
        if (instance.nextTimeMove > 0) {
            instance.nextTimeMove += -1;
        } else {
            if (instance.nextTimePause > 0) {
                instance.nextTimePause += -1;
                const newPositionX = instance.is.getPotentialX(instance);
                if (newPositionX !== instance.x) {
                    instance.x = newPositionX;
                    setXPosition(instance.dom, instance.x);
                }
            } else {
                instance.nextTimePause = instance.is.moveDuration;
                instance.nextTimeMove = instance.is.delayInBetweenMoves;
                instance.nextMoveDirection *= -1;
            }
        }

    },
    shoot: function (instance) {
        if (instance.nextTimeShoot > 0) {
            instance.nextTimeShoot += -1;
        } else {
            const speedX = rotatingValue(instance.incrementer);
            enemyBombPool.activate(
                instance.is.weapon,
                instance.x + instance.is.shootCenter,
                instance.y + instance.is.height,
                speedX * instance.weaponSpeed,
                (1 - Math.abs(speedX)) * instance.weaponSpeed + instance.weaponSpeedYAdder
            );
            instance.nextTimeShoot = instance.is.shootInterval;
            instance.incrementer += 1;
        }
    }
})));

const HardToCatch = addShip(softExtend(BaseShip, addShootCenter({
    name: "HardToCatch",
    id: "HardToCatch",
    points: 800,
    width: 40,
    height: 40,
    usesSpawn: false,
    weapon: DownFacingBomb,
    life: 1,
    shootCenter: 0,
    shootInterval: 100,
    weaponBaseSpeed: 0.68,
    weaponBaseSpeedYAdder: 0.22,
    delayInBetweenMoves: 80,
    moveDuration: 35,
    baseSpeedX: 1.5,
    speedX: 1.7,
    baseSpeedY: 0,
    speedY: 0,
    descriptor: {
       tagName: "div",
       "class": "boss-targeter"
    },
    create: function (x, y) {
        const instance = {
            is: HardToCatch
        };
        instance.dom = d.createElement2(instance.is.descriptor);
        instance.is.configure(instance, x, y);
        return instance;
    },
    configure: function (instance, x, y) {
        BaseShip.configure(instance, x, y);
        instance.nextTimeMove = instance.is.delayInBetweenMoves;
        instance.nextTimePause = instance.is.moveDuration;
        instance.incrementer = 0;
        instance.weaponSpeedYAdder = instance.is.weaponBaseSpeedYAdder * yMultiplier;
        instance.weaponSpeed =  instance.is.weaponBaseSpeed * yMultiplier;
    },
    getPotentialX: function (instance) {
        let xChanged = false;
        let maybeNewXPostion;
        /* need to have new x position to know if it will be in
        space conflict with another Object
        so to avoid situations where 2 things are inside each other*/
        if ((instance.x > player.x) && (player.turn === 1)) {
            maybeNewXPostion = instance.x + instance.is.speedX / gameState.slowedDivider;
            xChanged = true;
        } else if ((instance.x < player.x) && (player.turn === -1)) {
            maybeNewXPostion = instance.x - instance.is.speedX / gameState.slowedDivider;
            xChanged = true;
        }
        if (!xChanged) {
            return instance.x;
        }
        maybeNewXPostion = clampValue(
            maybeNewXPostion,
            /*min=*/0,
            /*max=*/width - instance.is.width
        );

        if (isNewPositionInConflictEnemyPool(instance.is.width, instance.is.height, maybeNewXPostion, instance.y, instance)) {
            return instance.x;
        }
        return maybeNewXPostion;
    },
    move: function (instance) {
        if (instance.nextTimeMove > 0) {
            instance.nextTimeMove += -1;
        } else {
            if (instance.nextTimePause > 0) {
                instance.nextTimePause += -1;
                const newPositionX = instance.is.getPotentialX(instance);
                if (newPositionX !== instance.x) {
                    instance.x = newPositionX;
                    setXPosition(instance.dom, instance.x);
                }
            } else {
                instance.nextTimePause = instance.is.moveDuration;
                instance.nextTimeMove = instance.is.delayInBetweenMoves;
            }
        }
    },
    shoot: function (instance) {
        if (instance.nextTimeShoot > 0) {
            instance.nextTimeShoot += -1;
        } else {
            const temp = Math.cos(instance.incrementer * 0.0625 * Math.PI);
            const temp2 = 1 - Math.abs(temp);
            enemyBombPool.activate(
                instance.is.weapon,
                instance.x + instance.is.shootCenter,
                instance.y + instance.is.height,
                temp * instance.weaponSpeed,
                temp2 * instance.weaponSpeed + instance.weaponSpeedYAdder
            );
            instance.nextTimeShoot = instance.is.shootInterval;
            instance.incrementer += 1;
        }

    }
})));

const WallShooter = addShip(softExtend(BaseShip, {
    name: "WallShooter",
    id: "WallShooter",
    weapon: WallBomb,
    points: 2000,
    width: 50,
    height: 50,
    usesSpawn: true,
    life: 2,
    shootInterval: 400,
    descriptor: {
       tagName: "div",
       "class": "wallshooter"
    },
    create: function (x, y) {
        const instance = {
            is: WallShooter
        };
        instance.dom = d.createElement2(instance.is.descriptor);
        instance.is.configure(instance, x, y);
        return instance;
    },
    shoot: function (instance) {
        if (instance.nextTimeShoot === 0) {
            enemyBombPool.activate(instance.is.weapon, instance.y + instance.is.height);
            instance.nextTimeShoot = instance.is.shootInterval;
        } else {
            instance.nextTimeShoot += -1;
        }
    }
}));

// all bodies that have
// baseSpeed but not speed yet
const unitsWithBaseSpeed = [
    Rotator,
    Dodger,
    HardToCatch,

    PlayerBomb,
    Bomb1,
    WallBomb,
    DiagonaleBomb,
    DroneMissile,
    ExplosionLauncher
];
