export {clickAndTouchMap};

const clickAndTouchMap = {
    g1: "slowTime",
    g2: "pause",
    g3: "shielder",
    g4: "swipeLeft",
    g5: "swipeBoard",
    g6: "swipeRight",
    g7: "left",
    g8: "shield",
    g9: "right",
};
