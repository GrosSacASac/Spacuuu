# Units

> Note this document gives an overview but is not up to date in every details

## Make a complete unit from scratch

> Some steps could be automated

Copy another unit in settings/units.js to have something to start from.

Change name, id and is. Change other things.

And to test it add it to levels.js also. Use the unit in "levels.js" with the "id" for that.

Add in to unitsWithBaseSpeed array if it has baseSpeed and you want it to have speed that scales to the screensize.

To add a new visual, change the descriptor.class create a svg, use that svg in game.css, add svg in buildAssets and Service worker.

see buildAssets.js
Optionally add a new weapon. the steps are the same.


```
Unit
    BaseBomb
        Bomb
    BaseShip
        Ship
```

To reduce tight coupling and to increase flexibility the Unit class does not exist at all.

## Bomb


A bomb is something shot from another ship,
it can collide with the player and move

### Required


```
const Bomb1 = {
    id: "Bomb1",
    width: 5,
    height: 18,
    maxAge: 1200,
    life: 18,
    descriptor: {
       tagName: "div",
       class: "bomb1"
    },
    create: function (...forConfigure) {
        /*
        should only set application-time constants
        example is and dom because it will not change
        */
        const instance = {
            is: Bomb1
        };
        instance.dom = d.createElement2(instance.is.descriptor);
        instance.is.configure(instance, ...forConfigure);
        return instance;
    }
};
```

### Optionals

Here are shown the default values


```
const Bomb1 = {
    baseSpeedX: 0,
    speedX: 0,
    baseSpeedY: 0,
    speedY: 0,
    move: function (instance) {
       /*
       called each tick, can update x, y positions
       if not provided speedX is used for linear movements
       */
    },
    cleanUp: function (instance) {
        /*
        reset things from previous use
        but not required in the first create()
        */
        instance.dom.classList.remove("targeted");
    },
    configure: function (instance, x, y, extras) {
        /*
        called when created or reactivated
        sets unit-time constants and initial values
        should set x, y values
        */
        instance.life = instance.is.life;
    },
    beforeDeActivated: function (instance) {
        /*
        before the unit is destroyed
        */
        scoreKill(instance.is.points);
    }
};
```


## Ship

A ship is usually a moving unit to be elminated to obtain point and advance to the next levels.


### Required


```
const EnemySimple = {
    name: "EnemySimple",
    id: "EnemySimple",
    points: 100,
    width: 25,
    height: 25,
    weapon: Bomb1,
    life: 1,
    shootInterval: 300,
    descriptor: {
       tagName: "div",
       "class": "enemy-simple"
    },
    create: function (...forConfigure) {
        /*
        should only set application-time constants
        example is and dom because it will not change
        */
        const instance = {
            is: EnemySimple
        };
        instance.dom = d.createElement2(instance.is.descriptor);
        instance.is.configure(instance, ...forConfigure);
        return instance;
    }
};
```



### Optionals


```
const EnemySimple = {
    baseSpeedX: 0,
    speedX: 0,
    baseSpeedY: 0,
    speedY: 0,
    shootCenter: 0,
    usesSpawn: true,
    shoot: function (instance) {
        if (instance.nextTimeShoot === 0) {
            enemyBombPool.activate(
                instance.is.weapon,
                instance.x + instance.is.shootCenter,
                instance.y + instance.is.height
            );
            instance.nextTimeShoot = instance.is.shootInterval;
        } else {
            instance.nextTimeShoot += -1;
        }
    },
    cleanUp: function (instance) {
        /*
        reset things from previous use
        but not required in the first create()
        */
        instance.dom.classList.remove("hurt");
    },
    configure: function (instance, x, y) {
        /*
        called when created or reactivated
        sets unit-time constants and initial values
        should set x, y values
        */
        instance.birthDate = tickCount;
        instance.x = x;
        instance.y = y;
    },
    beforeDeActivated: function (instance) {
        /*
        before the unit is destroyed
        */
        stopUseSpawn(spawnGrids[instance.is.width], instance.x);
        scoreKill(instance.is.points);
    }
};
```
