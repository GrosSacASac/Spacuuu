export {gamePadMap};

const gamePadMap = {
// A
    "0": "shield",
// B
    "1": "swipeRight",
// X
    "2": "swipeLeft",
// Y
    "3": "swipeBoard",
// back left
    "4": "slowTime",
// back right
    "5": "shielder",
// back
    "8": "throttledResumeOrPause",
// start
    "9": "throttledResumeOrPause"
};
