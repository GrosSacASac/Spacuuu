/* duration/pause pairs in a flat list
*/

export {deathPattern, hurtPattern, bossPattern};

const deathPattern = [
    320,100,
    260,100,
    200,100,
    140,100,
    40,80,
    40,80
];

const hurtPattern = [225];

const bossPattern = [
    120,150,
    120,130,
    120,100,
    120,80,
    120,60,
    120,800,
    120,800,
    500
];


