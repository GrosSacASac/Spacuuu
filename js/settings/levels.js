export { levels };

/*playerSetter: {
          life: 3,
          shields: 3,
          slowTime: 3,
          swipes: 2,
          nukes: 1,
          shielders: 2
        }*/

const testLevel = {
    name: "test level",
    id: "-1",
    steps: [
        {
            checkpoint: true,
            delayBefore: 0,
            blocking: true,
            action: "text",
            target: "Test.",
            duration: 1,
            playerSetter: {
                life: 1,
                shields: 100,
                slowTime: 100,
                swipes: 100,
                nukes: 100,
                shielders: 100
            }
        },
        {
            delayBefore: 50,
            blocking: true,
            action: "spawn",
            target: "ExplosionLauncher",
            count: 2
        },
        {
            delayBefore: 50,
            blocking: true,
            action: "spawn",
            target: "EnemyDiagonale",
            count: 1
        },
        {
            delayBefore: 50,
            blocking: true,
            action: "spawn",
            target: "Rotator",
            count: 6
        },
        {
            delayBefore: 50,
            blocking: true,
            action: "spawn",
            target: "HardToCatch",
            count: 2,
            fixedCount: true
        },
        {
            delayBefore: 50,
            blocking: true,
            action: "spawn",
            target: "Dodger",
            count: 4
        },
        {
            delayBefore: 50,
            blocking: true,
            action: "spawn",
            target: "ConfusingShooter",
            count: 1
        },
        {
            delayBefore: 50,
            blocking: true,
            action: "spawn",
            target: "BossSwiper",
            count: 1
        },
        {
            delayBefore: 50,
            blocking: true,
            action: "spawn",
            target: "BossTargeter",
            count: 1
        },
        {
            delayBefore: 50,
            blocking: true,
            action: "spawn",
            target: "EnemySimple",
            count: 8
        },
        {
            delayBefore: 50,
            blocking: true,
            action: "spawn",
            target: "WallShooter",
            count: 1
        },
        {
            delayBefore: 30,
            blocking: true,
            action: "text",
            target: "test finish.",
            duration: 30
        }
    ]
};

const tutorialLevel = {
    name: "Tutorial",
    id: "0",
    next: "1",
    steps: [
        {
            checkpoint: true,
            delayBefore: 0,
            blocking: true,
            action: "text",
            target: "Welcome to the tutorial. Click or touch the highlighted areas to follow along.",
            duration: 260,
            playerSetter: {
                life: 30,
                shields: 0,
                slowTime: 0,
                swipes: 0,
                nukes: 0,
                shielders: 0
            }
        },
        {
            checkpoint: true,
            delayBefore: 0,
            blocking: true,
            action: "text",
            target: "Press the top center or P at any moment to pause.",
            duration: 320,
            classChange: ["g2"]
        },
        {
            delayBefore: 0,
            blocking: true,
            action: "text",
            target: "Bottom left and bottom right or arrows to move.",
            duration: 320,
            classChange: ["g7", "g9"]
        },
        {
            delayBefore: 0,
            blocking: true,
            action: "text",
            target: "Try it.",
            duration: 50,
            classChange: ["g7", "g9"]
        },
        {
            delayBefore: 50,
            blocking: true,
            action: "spawn",
            target: "EnemySimple",
            count: 2,
            fixedCount: true
        },
        {
            delayBefore: 10,
            blocking: true,
            action: "text",
            target: "Well Done.",
            duration: 60
        },
        {
            checkpoint: true,
            delayBefore: 0,
            blocking: true,
            action: "text",
            target: "Bottom Center or down arrow to activate a shield.",
            duration: 300,
            classChange: ["g8"],
            playerSetter: {
                life: 1,
                shields: 20,
                shielders: 2 // to remove
            }
        },
        {
            delayBefore: 50,
            blocking: true,
            action: "spawn",
            target: "WallShooter",
            count: 1,
            fixedCount: true
        },
        {
            checkpoint: true,
            delayBefore: 100,
            blocking: true,
            action: "text",
            target: "Center of the screen or Enter key to launch a big bomb when there is too much",
            duration: 300,
            classChange: ["g5"],
            playerSetter: {
                life: 1,
                shields: 1,
                nukes: 10,
            }
        },
        {
            delayBefore: 30,
            blocking: true,
            action: "spawn",
            target: "EnemySimple",
            count: 9
        },
        {
            delayBefore: 100,
            blocking: true,
            action: "text",
            target: "Tutorial finished.",
            duration: 240
        }
    ]
};

const level1 = {
    name: "Level1",
    id: "1",
    next: "2",
    steps: [
        {
            delayBefore: 0,
            blocking: true,
            action: "text",
            target: `This is level 1!`,
            duration: 30
        },
        {
            delayBefore: 30,
            blocking: true,
            action: "spawn",
            target: "EnemySimple",
            count: 1
        },
        {
            delayBefore: 30,
            blocking: true,
            action: "text",
            target: `End of level1 !`,
            duration: 60
        },
    ],
};

const level2 = {
    name: "Level2",
    id: "2",
    next: undefined,
    steps: [
        {
            delayBefore: 0,
            blocking: true,
            action: "text",
            target: `This is level 2!`,
            duration: 30
        },
        {
            delayBefore: 30,
            blocking: true,
            action: "spawn",
            target: "EnemySimple",
            count: 1
        },
        {
            delayBefore: 30,
            blocking: true,
            action: "text",
            target: `End of level 2 !`,
            duration: 60
        },
    ],
};


const levels = {
    [testLevel.id]: testLevel,
    [tutorialLevel.id]: tutorialLevel,
    [level1.id]: level1,
    [level2.id]: level2,
};
