export {getRandomErrorMessage};

const randomErrorMessages = [
    "You cannot travel at a high enough speed to traverse a parallel universe !",
    "Teleportation has not been invented yet !",
    "How often do I have to tell you: Time-travel is only in fictions !"
];

let i = -1;
const getRandomErrorMessage = function () {
    i = (i + 1) % randomErrorMessages.length;
    return randomErrorMessages[i];
}