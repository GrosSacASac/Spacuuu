export { keyBoardMap };

const keyBoardMap = {
    // left arrow
    "37": "left",
    // right arrow
    "39": "right",
    // down arrow
    "40": "shield",
    // control
    "17": "swipeLeft",
    // 0 numpad
    "96": "swipeRight",
    // Enter
    "13": "swipeBoard",
    // shift
    "16": "slowTime",
    // up arrow
    "38": "shielder",
    // P
    "80": "throttledResumeOrPause",
    // esc
    "27": "throttledResumeOrPause"
};
