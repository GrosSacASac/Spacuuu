export { initialGameState };

const initialGameState = {
    slowTimeEndFrame: 0,
    slowedDivider: 1, // 1 or  2 (divide by 2 and speed will be as slowed
    slowedIncrement: 1, // 1 or 0 (slowed incrementer)
    slowTimeActive: false,
    swipeLeftActive: false,
    swipeRightActive: false,
    swipeLeftEndFrame: 0,
    swipeRightEndFrame: 0,
    shielderActive: false,
    shielderLastTarget: undefined,
    shielderTimeRemaining: 0
};