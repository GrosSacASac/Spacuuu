/*localSave.js*/
/*
Starts by reading what was previously saved inside localStorage when imported
very tolerant and does not produce errors if localStorage is disabled
asks before first time local storing
can get data from it the same way if it is actually stored or not
acts as a data store, read redux

most of the functions here return true when the save was success full


    
todo make storage more consistent when multiple tabs are open
    or give that responsability to the application
	also update strategy
*/
export { explicitSave, getFast,  getElseDefault, getElseSetDefault, setFast, save, setAndSave, clear, setAndSaveSilent, clearAllData};
import {yesNoDialog, textDialog}  from "../node_modules/dom99/components/yesNoDialog/yesNoDialog.js";

const version = `6.0.0`;
const STORE_NAME = `SPACUUU`;
let allowedToUseLocalData = undefined;
let canUseLocalData = Boolean(window.localStorage);
let fastCopy;

(function (itemName) {
    const getFreshStorage = {
                version,
                STORE_NAME
            };
    if (!canUseLocalData) {
        fastCopy = getFreshStorage;
        return;
    }
    try {
        const fastCopyString = localStorage.getItem(STORE_NAME);
        if (!fastCopyString) {
            // null or String
            // Empty
            fastCopy = getFreshStorage;
        } else {
            fastCopy = JSON.parse(fastCopyString);
            // we could also parse null directly for browsers that support the new JSON spec
            if (!fastCopy) {
                // Empty 
                fastCopy = getFreshStorage;
            } else {
                // check if there is already data saved
                // this is not strictly necessary we could just
                // allowedToUseLocalData = true; here too
                if (fastCopy.hasOwnProperty("STORE_NAME") && fastCopy.STORE_NAME === STORE_NAME) {
                    // there is , which means already allowed
                    allowedToUseLocalData = true;
                }
                    
                if (fastCopy.version !== version) {
                    fastCopy = getFreshStorage;
                    // not ideal as it resets everything but works
                    // could update things on a per field basis instead
                }
            }
        }

    } catch (e) {
        /* maybe there was no data, 
        or stored differently before so it breaks JSON.parse
        clear all data and returns undefined */
        canUseLocalData = false;
        allowedToUseLocalData = false;
        fastCopy = getFreshStorage;
    }
 }());

const getFast = function (key) {
    if (Object.prototype.hasOwnProperty.call(fastCopy, key)) {
        return fastCopy[key];
    }
    return undefined;
};  

const setFast = function (key, value) {
    fastCopy[key] = value;
    return value;
};
  
const getElseDefault = function (key, defaultValue) {
    const value = getFast(key);
    if (value === undefined) {
        return defaultValue;
    }
    return value;
};

const getElseSetDefault = function (key, defaultValue) {
    const value = getFast(key);
    if (value === undefined) {
        setFast(key, defaultValue);
        return getFast(key);
    }
    return value;
};
    
const clearAllData = function () {
    return yesNoDialog(`Delete all data ? This cannot be undone.`, `Delete All`, `No, Cancel`).then(function (reallyWants) {
        if (reallyWants) {
            clear();
        }
        return reallyWants;
    });
};

let saveSilent;
let save;
let setAndSaveSilent;
let setAndSave;
let clear;
if (canUseLocalData) {
    saveSilent = function () {
        if (allowedToUseLocalData) {
            localStorage.setItem(STORE_NAME, JSON.stringify(fastCopy));
            return true;
        }
        return false;
    };

     save = function () {
        if (allowedToUseLocalData === undefined) {
            return yesNoDialog(`Allow settings, saves and scores to be saved on the computer ?`,
                `Yes`,
                `No`).then(function (answer) {
                allowedToUseLocalData = answer;
                return saveSilent();
          });
        } else {
            return Promise.resolve(saveSilent());
        }
      };

      
        setAndSaveSilent = function (key, value) {
          setFast(key, value);
          return saveSilent();
      };
      
    setAndSave = function (key, value) {
        setFast(key, value);
        return save();
    };

    clear = function () {
        localStorage.removeItem(STORE_NAME);
        // localStorage.clear();
    };
} else {
    const doNothing = function () {};
    const returnEmptyPromise = function () {
        return Promise.resolve(false);
    };
    saveSilent = doNothing;
    save = returnEmptyPromise;
    setAndSaveSilent = setFast;
    setAndSave = function (key, value) {
        setFast(key, value);
        return returnEmptyPromise();
    };
    clear = doNothing;
}

     
const explicitSave = function () {
    // the user tries to save from himself
    if (!canUseLocalData) {
        return yesNoDialog(`Your device, system or settings do not allow localstorage. Do you understand ?`,
            `Yes`,
            `No`).then(function (_) {

            return false;
        });
    }
    // not the same if statement as in save()
    if (!allowedToUseLocalData) {
        return yesNoDialog(`Allow settings, saves and scores to be saved on the computer ?`,
            `Yes`,
            `No`).then(function (answer) {
            allowedToUseLocalData = answer;
            return saveSilent();
      });
    } else {
        return Promise.resolve(saveSilent());
    }
};

console.log(`Initial store from localStorage:`);
console.log(fastCopy);