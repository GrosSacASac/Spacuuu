import {getFast,  getElseDefault, getElseSetDefault, setFast, save, setAndSave, clear, setAndSaveSilent, explicitSave} from "./localSave.js";
import {levels} from "./settings/levels.js";
import {yesNoDialog, textDialog}  from "../node_modules/dom99/components/yesNoDialog/yesNoDialog.js";
export {saveScore, scoreUsed60Frames, scoreLoseLife, scoreKill, getFinalScore, getScore, setScore, scoreStartLevel, scoresForDisplay};

const placeHolderName = `No name given`;
const maxScores = 5;
let score = 0;

const scoreStartLevel = function () {
  score = 1000;
};

const getScore = function () {
  return score;
};

const setScore = function (initialScore) {
  score = initialScore;
};

const scoreUsed60Frames = function () {
  score -= 1;
};

const scoreLoseLife = function () {
    score -= 10;
};

const scoreKill = function (killReward) {
 score += killReward;
};

const getFinalScore = function () {
  return Math.max(0, getScore());
};


const scoresForDisplay = function (scores) {
  /* uses what saveScore did*/
  // use .sort() on the result of map to also sort
  return Object.entries(scores).map(function ([dimensions, scoreByDimension]) {
        return {
            dimensions,
            levels: Object.values(scoreByDimension)
      };
  });
};

const saveScore = function (levelId, dimensions) {
  const finalScore = getFinalScore();
  const allScores = getElseSetDefault(`scores`, {});
  
  if (allScores[dimensions]) {
    const allScoresForThisDimension = allScores[dimensions];
    if (allScoresForThisDimension[levelId]) {
      const scoresForLevel = allScoresForThisDimension[levelId].scores;
      let smallestScore;
      if (scoresForLevel.length === 0) {
          smallestScore = -1;
      } else {
            smallestScore = scoresForLevel.reduce(function (smallest, current) {
                // cannot pass Math.min directly
                // because there are extra parameters
                return Math.min(smallest, current.score);
            }, Number.MAX_SAFE_INTEGER);
      }

      if (finalScore > smallestScore || scoresForLevel.length < maxScores) {
      
        textDialog(`Nice your score ${finalScore} is in the top ${maxScores}  !`, `Pseudonym ?`, getElseDefault(`pseudo`, ``), `Save Score`).then(function (input) {
            document.activeElement.blur();
            d.elements.body.focus();
            let playerName;
            if (input) {
              // for ie
              if (input.normalize !== undefined) {
                playerName = input.normalize();
              } else {
                playerName = input;
              }
            } else {
              playerName = placeHolderName;
            }
            setFast(`pseudo`, playerName);
            // max because it is -1 when empty
            const finalScoreObject = {
              player: playerName,
              score: finalScore
            };
            
            if (scoresForLevel.length === 0) {
                scoresForLevel.push(finalScoreObject);
            } else {
                let indexToAdd = scoresForLevel.findIndex(function (scoreDescription) {
                  return finalScore > scoreDescription.score;
                });
                if (indexToAdd === -1) {
                    // lowest score add at the end 
                    indexToAdd = maxScores - 1;
                }
                scoresForLevel.splice(indexToAdd, 0, finalScoreObject);
                if (scoresForLevel.length > maxScores) {
                  scoresForLevel.length = maxScores; // cut the last
                }
            }
            save();
        });
      } else {
        console.log(`Score is lower than the top ${maxScores}.`,
            `smallestScore ${smallestScore}.`,
            `finalScore ${finalScore}.`);
      }
    } else {
      allScoresForThisDimension[levelId] = {
        level: levels[levelId].name,
        scores: []
      };
      saveScore(levelId, dimensions);
    }
  } else {
    allScores[dimensions] = {};
    saveScore(levelId, dimensions);
  }
};
