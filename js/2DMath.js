export {
    rotatingValue, angleFromVector,
    clampValue,insideAxis,createSpawnGrids,
    useRandomSpawn, useSpawn, stopUseSpawn, randomSpawn
};

const minMargin = 1;

const sizes = [15, 20, 25 , 50, 40, 130];

const stepPi = (1 / 16) * Math.PI;
// this ensure we go through a wide range of values
const rotatingValue = function (i) {    
    return Math.cos(i * stepPi);
};

const clampValue = function (value, min, max) {
    /* return a value that is in between min and max and
    as close as possible to value
    advantages over using if else to clamp a value:
    it goes as close as possible to the limit */
    
    return Math.max(min, Math.min(max, value));
};

const angleFromVector = function (x, y, angleOffSet = 0) {    
    return Math.atan2(y, x) + angleOffSet;
};

const createSpawnGrid = function (size, margin, max) {
    const maxSafe = max - size;
    const space = margin + size;
    const grid = [];
    const used = []; // initially empty
    const unused = []; // initially full
    let position = margin;
    let i;
    for (i = 0; position < maxSafe; position += space) {
        grid.push(position);
        unused.push(i);
        i += 1;
    }
    
    // having 2 more grids makes it very convenient to get a random spawn
    // without having to check if it is already used in a while loop
    // and repeat random ...
    return {
        grid,
        used,
        unused,
        size: i
    };
};

const createSpawnGrids = function (multiplier, max) {
    const marginScaled = Math.round(minMargin * multiplier);
    
    const grids = {};
    sizes.forEach(function (size) {
        grids[size] = createSpawnGrid(size, marginScaled, max)
    });
    return grids;
};

const useSpawn = function (spawnGrid, index) {
    // index here is a value
    // but it is also the index for .grid that points to a position
    const indexIndex = spawnGrid.unused.indexOf(index);
    spawnGrid.unused.splice(indexIndex, 1);
    spawnGrid.used.push(index);
    const position = spawnGrid.grid[index];
    return position;
};

const stopUseSpawn = function (spawnGrid, position) {
    const index = spawnGrid.grid.indexOf(position);
    const indexIndex = spawnGrid.used.indexOf(index);
    spawnGrid.used.splice(indexIndex, 1);
    spawnGrid.unused.push(index);
    return position;
};

const useRandomSpawn = function (spawnGrid) {
    const possibilities = spawnGrid.unused.length;
    const randomIndexIndex = Math.floor(Math.random() * possibilities);
    const randomIndex = spawnGrid.unused[randomIndexIndex];
    return useSpawn(spawnGrid, randomIndex);
};

const randomSpawn = function (spawnGrid) {
    // like useRandomSpawn but
    // does not remember that the position is used
    // for moving targets
    const possibilities = spawnGrid.unused.length;
    const randomIndexIndex = Math.floor(Math.random() * possibilities);
    const randomIndex = spawnGrid.unused[randomIndexIndex];
    return spawnGrid.grid[randomIndex];
};

const insideAxis = function (start1, length1, start2, length2) {
    /* start1: a point in a 1D space, length1
        returns true if both vectors collide 
    */
    const end1 = start1 + length1;
    const end2 = start2 + length2;
    return (
        (start1 <= end2) && (end1 >= start2)
    );
};

