/*
https://developer.mozilla.org/en-US/docs/Web/CSS/angle

*/

export {
    removeDom, addDom, setXPosition, setYPosition,
    setXYPosition,
    focusNext, focusPrevious,
    setDegreeAngle, setRadianAngle,
    hide, show
};
import * as d from "../node_modules/dom99/built/dom99ES.js";
import {tabbable} from "./external_dependencies/tabbable.js";

const removeDom = function (object) {
    object.dom.remove();
};

const hide = function (dom) {
    dom.hidden = true;
};

const show = function (dom) {
    dom.hidden = false;
};

const setXPosition = function (dom, x) {
    dom.style.left = `${x}px`;

    // dom.x = x;
    // dom.style.transform = `translate(${x}px, ${dom.y}px)`;

};

const setYPosition = function (dom, y) {
    dom.style.top = `${y}px`;

    // dom.y = y;
    // dom.style.transform = `translate(${dom.x}px, ${y}px)`;

};

const setXYPosition = function (dom, x, y) {
    dom.style.top = `${y}px`;
    dom.style.left = `${y}px`;

    // dom.y = y;
    // dom.x = x;
    // dom.style.transform = `translate(${x}px, ${y}px)`;

};


const setDegreeAngle = function (dom, degrees) {
    dom.style.transform = `rotate(${degrees}deg)`;
};

const setRadianAngle = function (dom, radians) {
    dom.style.transform = `rotate(${radians}rad)`;
};


const addDom = function (dom, y) {
    d.elements.playground.appendChild(dom);
};

const focusNextInList = function (element, list) {
    let toFocus;
    if (!element) {
        toFocus = list[0];
    } else {
        const nextIndex = (1 + list.indexOf(element)) % list.length;
        toFocus = list[nextIndex];
    }
    toFocus.focus();
};

const focusNext = function (element = document.activeElement) {
    const list = tabbable(document.body);
    focusNextInList(element, list);
};

const focusPrevious = function (element = document.activeElement) {
    const list = tabbable(document.body).reverse(); // !!!
    focusNextInList(element, list);
};
