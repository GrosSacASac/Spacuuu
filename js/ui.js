import * as d from "../node_modules/dom99/built/dom99ES.js";
import { yesNoDialog, textDialog } from "../node_modules/dom99/components/yesNoDialog/yesNoDialog.js";
import fscreen from "../node_modules/fscreen/src/index.js";
import {
    createThrottled,
    doNTimes,
    chainPromises,
    chainPromiseNTimes,
    timeFunction,
    timePromise,
    arrayWithResults
} from "../node_modules/utilsac/utility.js";

import {
    setXPosition, setYPosition, focusNext, focusPrevious,
    hide, show
} from "./dom.js";
import {
    wipeBoard
} from "./bodies.js";
import { player, Ships } from "./settings/units.js";
import { enemyPool, enemyBombPool, alliedBombPool } from "./pools.js";
import {
    switchTrack, loadSounds, pauseTrack, resumeTrack
} from "./sound.js";
import {
    speechSynthesisSupport,
    speak, stopSpeak, resumeSpeak
} from "./speechSynthesis.js";
import {
    historySupport,
    historyPush,
    historyBack,
    historyReplace,
    historyBackAndReplace
} from "./history.js";
import { vibrationSupport, vibrate, vibrateForPermission } from "./vibration.js";
import { deathPattern, hurtPattern, bossPattern } from "./settings/vibrationPatterns.js";
import { levels } from "./settings/levels.js";
import {
    getFast, getElseDefault, getElseSetDefault, setFast,
    save, setAndSave, clearAllData, setAndSaveSilent, explicitSave
} from "./localSave.js";
import {
    saveScore,
    scoresForDisplay
} from "./scores.js";
import { createSpawnGrids, useRandomSpawn, useSpawn, randomSpawn } from "./2DMath.js";
import { gamePadSupport, getGamePadInput, lastGamePadInput, gamePadMap } from "./gamepads.js";
import { lastClickAndTouchInput, clickAndTouchMap, resetClickAndTouch } from "./clickAndTouch.js";
import { lastKeyBoardInput, keyBoardMap, resetKeyBoard } from "./keyBoard.js";
import { serviceWorkerSupport, startServiceWorker, deleteServiceWorker } from "./service_worker/serviceWorkerManager.js";
import { getRandomErrorMessage } from "./settings/texts.js";
import {
    startLevel, startMultiplayer, startMultiplayerLevel,
    pause, resume, inGame, paused, resetGame, loadCheckPointTransition, l,
    quitTransition, defaultOptionsKeys,
    allSounds
} from "./main.js";
export {
    listenForGamePadMenu, updateScoresMenu,
    updateLoadPossibility, restoreHistory,
    throttledResumeOrPause, setGraphics,
    dispatchAction
};

let ui;
/*
a state is a string that describes a situation
an action is emitted by an event, it is an object with at least a type
a transition is a function that takes a state an action and returns a new state
it guarantees that the real state reflect the new state before it returns
a stateMachine is a mediator to make an action do the right transition
currentState is a variable that always holds the state we are in
dispatchAction(action) is a short-cut function for
currentState = stateMachine(currentState, action);
*/
/*
const allStates = {
    state0: {
        action0: `transition0`,
        ...
    },
    state1: {
        action1: `transition1`,
        ...
    }
};
*/

const MENU_THROTTLE_DELAY = 175;
const MENU_THROTTLE_SELECT_DELAY = 900;
const focusNextThrottled = createThrottled(focusNext, MENU_THROTTLE_DELAY);
const focusPreviousThrottled = createThrottled(focusPrevious, MENU_THROTTLE_DELAY);

const hideAll = function () {
    [
        d.elements.mainMenu,
        d.elements.loadMenu,
        d.elements.optionsMenu,
        d.elements.scoresMenu,
        d.elements.helpMenu,
        d.elements.pauseMenu,
        d.elements.multipleInputsMenu
    ].forEach(hide);
};

/* make this allTransitions ?*/
const someTransitions = {
    startLevelSelection: function (currentState, action) {
        historyPush({ inside: `pause` });
        startLevelSelection(); // also historyPush
        return `levelSelection`;
    },
    startLevel: function (currentState, action) {
        historyPush({ inside: `pause`, levelId: action.id });
        startLevel(action.id); // also historyPush
        return `game`;
    },
    startMultiplayerLevel: function (currentState, action) {
        historyPush({ inside: `pause` });
        startMultiplayerLevel(action);
        return `game`;
    },
    startMultiplayer: function (currentState, action) {
        historyPush({ inside: `multiplayer` });
        startMultiplayer();
        return `multiplayer`;
    },
    openLoad: function (currentState, action) {
        show(d.elements.loadMenu);
        hide(d.elements.mainMenu);
        historyPush({ inside: `load` });
        return `load`;
    },
    openOptions: function (currentState, action) {
        show(d.elements.optionsMenu);
        hide(d.elements.mainMenu);
        hide(d.elements.pauseMenu);
        historyPush({ inside: `options` });
        return `options`;
    },
    openHelp: function (currentState, action) {
        show(d.elements.helpMenu);
        hide(d.elements.mainMenu);
        historyPush({ inside: `help` });
        return `help`;
    },
    openScores: function (currentState, action) {
        const scores = getFast(`scores`);
        d.feed(`allScores`, scoresForDisplay(scores));
        show(d.elements.scoresMenu);
        hide(d.elements.mainMenu);
        historyPush({ inside: `scores` });
        return `scores`;
    },
    back: function (currentState, action) {
        hideAll();
        if (
            (currentState === `scores`) ||
            (currentState === `help`) ||
            (currentState === `load`) ||
            (currentState === `multiplayer`) ||
            (currentState === `pause`) ||
            ((currentState === `options`) &&
                !inGame)
        ) {
            show(d.elements.mainMenu);
            if (action.intent === `restoreHistory`) {
                historyBackAndReplace({ inside: `main` });
            } else {
                historyReplace({ inside: `main` });
            }
            return `main`;
        } else if (
            currentState === `options` &&
            inGame
        ) {
            show(d.elements.pauseMenu);
            if (action.intent === `restoreHistory`) {
                historyBackAndReplace({ inside: `pause` });
            } else {
                historyReplace({ inside: `pause` });
            }
            return `pause`;
        } else {
            console.error(`should not be here`);
        }
    },
    backFromPauseMenu: function (currentState, action) {
        quitTransition();
        if (action.intent === `restoreHistory`) {
            historyBackAndReplace({ inside: `main` });
        } else {
            historyReplace({ inside: `main` });
        }
        return `main`;
    },
    backFromGame: function (currentState, action) {
        if (Object.prototype.hasOwnProperty.call(action, `reason`) && action.reason === `death`) {
            d.elements.resume.disabled = true;
        }
        pause();
        return `pause`;
    },
    backFromMain: function (currentState, action) {
        d.feed(`welcomeText`, getRandomErrorMessage());
        d.feed(`welcomeSubText`, ``);
        historyReplace({ inside: `main` });
        historyReplace({ inside: `main` });
        return `main`;
    },
    loadGame: function (currentState, action) {
        loadCheckPointTransition();

        return `game`;
    },
    resume: function () {
        if (resume()) {
            historyPush({ inside: `game` });
            return `game`;
        } else {
            l(`could not resume`);
            return `pause`;
        }
    }
};

const allStates = {
    main: {
        startLevel: someTransitions.startLevel,
        startMultiplayer: someTransitions.startMultiplayer,
        openLoad: someTransitions.openLoad,
        openOptions: someTransitions.openOptions,
        openScores: someTransitions.openScores,
        openHelp: someTransitions.openHelp,
        back: someTransitions.backFromMain
    },
    game: {
        back: someTransitions.backFromGame
    },
    pause: {
        back: someTransitions.backFromPauseMenu,
        resume: someTransitions.resume,
        openOptions: someTransitions.openOptions,
        loadGame: someTransitions.loadGame
    },
    load: {
        back: someTransitions.back,
        loadGame: someTransitions.loadGame,
    },
    options: {
        back: someTransitions.back
    },
    scores: {
        back: someTransitions.back
    },
    help: {
        back: someTransitions.back,
        startLevel: someTransitions.startLevel
    },
    multiplayer: {
        back: someTransitions.back,
        startMultiplayerLevel: someTransitions.startMultiplayerLevel
    }
};

const initialState = `main`;
let currentState = initialState;

const stateMachine = function (currentState, action) {
    /*
    takes currentState and action
    immediately does things to get in the new state
    then returns a new state that describes the exact situation we are in
    */
    // console.log(`currentState:`, currentState);
    // console.log(`action:`, action);
    let actionType;
    if (typeof action === `string`) {
        actionType = action;
    } else {
        actionType = action.type;
    }
    let newState;
    if (Object.prototype.hasOwnProperty.call(allStates[currentState], actionType)) {
        newState = allStates[currentState][actionType](currentState, action);
    } else {
        newState = currentState;
        console.warn(currentState, `does not have action`, action);
    }

    console.log(`newState:`, newState);
    return newState;
};

const dispatchAction = function (action) {
    currentState = stateMachine(currentState, action);
};

const restoreHistory = function (stateToRestore = {}) {
    if (currentState === `main`) {
        dispatchAction({ type: `back`, intent: `restoreHistory` });
        return;
    }
    if (stateToRestore.inside === `game`) {
        if (inGame && paused) {
            d.functions.resume();
        } else {
            console.log(`cannot go back to a game that is finished`);
            // going more back
            historyBack();
        }
    } else {
        dispatchAction({ type: `back`, intent: `restoreHistory` });
    }
};



d.functions.openLoad = function () {
    dispatchAction(`openLoad`);
};

d.functions.openOptions = function () {
    dispatchAction(`openOptions`);
};

d.functions.openHelp = function () {
    dispatchAction(`openHelp`);
};

d.functions.openScores = function () {
    dispatchAction(`openScores`);
};

d.functions.quitLoadMenu = function () {
    dispatchAction({ type: `back` });
};

d.functions.quitScoresMenu = function () {
    dispatchAction({ type: `back` });
};

d.functions.quitMultiplayerMenu = function () {
    dispatchAction({ type: `back` });
};

d.functions.quitHelpMenu = function () {
    dispatchAction({ type: `back` });
};

d.functions.quitOptionsMenu = function () {
    dispatchAction({ type: `back` });
};

d.functions.tutorial = function () {
    dispatchAction({ type: `startLevel`, id: `0` });
};

d.functions.start = function () {
    dispatchAction({ type: `startLevel`, id: `1` });
};

d.functions.startMultiplayer = function () {
    dispatchAction({ type: `startMultiplayer`, id: `1` });
};

d.functions.startTest = function () {
    dispatchAction({ type: `startLevel`, id: `-1` });
};

d.functions.loadCheckPoint = function () {
    dispatchAction({ type: `loadGame` });
};


const offerLastCheckPointReturn = function () {
    d.elements.loadCheckPoint.hidden = false;
    d.elements.resume.hidden = true;
};


d.functions.saveCheckPoint = function () {
    // we already have set, the correct playstate
    // from back then, only need to save (not silent)
    explicitSave().then(function (hasSaved) {
        if (hasSaved) {
            lastSave.lastCheckPointSaved = true;
            afterSaveCheckPoint();
        }
    });
};

const afterSaveCheckPoint = function () {
    d.elements.saveCheckPoint.disabled = true;
    d.feed(`saveCheckPoint`, `Checkpoint Saved !`);
};


d.functions.pToResume = function (event) {
    const keyCode = event.keyCode;
    if (keyCode === 80) { // P
        throttledResumeOrPause();
    }
}

const throttledResumeOrPause = createThrottled(function () {
    if (paused) {
        dispatchAction(`resume`);
    } else {
        dispatchAction({ type: `back` });
    }
}, MENU_THROTTLE_SELECT_DELAY);


d.functions.restart = function () {
    resetGame();
    dispatchAction({ type: `resume` });
};

const updateLoadPossibility = function () {
    d.elements.load.disabled = !Boolean(getFast(`autoSave`));
};

const updateScoresMenu = function () {
    const scores = getFast(`scores`);
    d.elements.openScores.disabled = !Boolean(scores);
};


const processGamePadInputsMenu = function (menuEventTime) {
    // console.log(lastGamePadInput);
    const controllerPlayer1 = lastGamePadInput.controllers[0];
    if (!controllerPlayer1) {
        return;
    }
    for (let i = 0; i < controllerPlayer1.buttons.length; i += 1) {
        const {/*value, */pressed } = controllerPlayer1.buttons[i];
        if (pressed) {
            // console.log(i);
            // implicit to string conversion
            if (gamePadMenuMap.hasOwnProperty(i)) {
                gamePadMenuMap[i]();
            }
        }
    }


    for (let i = 0; i < controllerPlayer1.axes.length; i += 1) {

        const value = controllerPlayer1.axes[i];
        if (value > 0) {
            focusNextThrottled();
        } else if (value < 0) {
            focusPreviousThrottled();
        }/* else if (distance === 0){
            // keep same direction player.turn
        }*/
    }


};

const menuSelect = createThrottled(function (NowTime) {
    try {
        document.activeElement.click();
    } catch (error) {

        console.error(error);
    }
}, MENU_THROTTLE_SELECT_DELAY);

const gamePadMenuMap = {

    // A
    "0": menuSelect,
    // back
    "8": throttledResumeOrPause,
    // start
    "9": throttledResumeOrPause,
    // top
    "12": focusPreviousThrottled,
    // bottom
    "13": focusNextThrottled,
    // left
    "14": focusPreviousThrottled,
    // right
    "15": focusNextThrottled
};

const listenForGamePadMenu = function (NowTime) {
    if (!paused) {
        return;
    }

    getGamePadInput();
    processGamePadInputsMenu(NowTime);
    requestAnimationFrame(listenForGamePadMenu);
};

d.functions.removeEverything = function () {
    clearAllData().then(function (reallyWants) {
        if (reallyWants) {
            /* also remove cookies (now) there is none
                ...
            */
            deleteServiceWorker();
            location.href = location.href;
        }
    });
};

d.functions.soundChange = function () {
    if (d.variables.sound) {
        loadSounds(allSounds);
    }
    d.functions.changeOption();
};

d.functions.musicChange = function () {
    /* play menu music */
    d.functions.changeOption();
};

d.functions.speechSynthesisChange = function () {
    if (d.variables.speechSynthesis) {
        speak(`Speech Synthesis enabled.`);
    }
    d.functions.changeOption();
};

d.functions.vibrationChange = function () {
    if (d.variables.vibrations) {
        // forces ask permissions
        vibrateForPermission() || vibrate(bossPattern);
    }
    d.functions.changeOption();
};

d.functions.offlineChange = function () {
    if (d.variables.offline) {
        d.elements.offlineCheckBox.disabled = true;
        show(d.elements.offlineProgress);
        show(d.elements.offlineNotes);
        d.feed(`offlineNotes`, `It may take a few seconds before the game is fully downloaded.`);
        const statusUpdater = function (count, total) {
            d.elements.offlineProgress.max = total;
            d.feed(`offlineProgress`, count);
        };
        const finallyStep = function () {
            // finally
            d.elements.offlineCheckBox.disabled = false;
            hide(d.elements.offlineProgress);
        };
        startServiceWorker(statusUpdater).then(function () {
            d.feed(`offlineNotes`, `Ready for offline use.`);
            finallyStep();
        }).catch(function (reason) {
            d.feed(`offlineNotes`, `A problem occured: ${reason}`);
            d.feed(`offline`, false);
            d.functions.changeOption();
            finallyStep();
        });
    } else {
        deleteServiceWorker();
        hide(d.elements.offlineNotes);
    }
    d.functions.changeOption();
};

const setGraphics = function (setting) {
    if (setting === `0`) {
        d.elements.body.classList.add(`low`);
    } else {
        d.elements.body.classList.remove(`low`);
    }
};

d.functions.graphicsChange = function () {
    setGraphics(d.variables.graphics);
    d.functions.changeOption();
};

d.functions.changeOption = function () {
    const allOptions = getElseSetDefault(`options`, {});
    defaultOptionsKeys.forEach(function (key) {
        allOptions[key] = d.variables[key];
    });

    save();
};

d.functions.changeInputMode = function (event) {
    const scope = d.scopeFromEvent(event);
    const newInput = d.variables[d.scopeFromArray([scope, `inputMode`])];
    setAndSaveSilent(scope, { inputMode: newInput });
};


const goFullScreen = function () {
    if (fscreen.fullscreenElement) {
        // already in full screen
    } else {
        // not yet in full screen
        fscreen.requestFullscreen(document.documentElement);
        // d.feed(`Exit Full Screen`,`nextToggleFullScreen`);
    }
};

d.functions.toggleFullScreen = function () {
    if (fscreen.fullscreenElement) {
        // already in full screen
        fscreen.exitFullscreen();
        // d.feed(`Go Full Screen`,`nextToggleFullScreen`);
    } else {
        // not yet in full screen
        fscreen.requestFullscreen(document.documentElement);
        // d.feed(`Exit Full Screen`,`nextToggleFullScreen`);
    }
};

d.functions.preventContextMenu = function (event) {
    event.preventDefault();
    return false;
};
