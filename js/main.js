import * as d from "../node_modules/dom99/built/dom99ES.js";
import { useYesNoDialog } from "../node_modules/dom99/components/yesNoDialog/yesNoDialog.js";
import { setXPosition, setYPosition, setXYPosition, focusNext, focusPrevious, show, hide } from "./dom.js";
import fscreen from "../node_modules/fscreen/src/index.js";
import { doNTimes } from "../node_modules/utilsac/utility.js";
import {
    enemiesUpdate, bombsUpdate, resetAllBodies, playerUpdate, wipeBoard, softActivate, lateActivate, delayedPool
} from "./bodies.js";
import { player, Player, Players, Ships } from "./settings/units.js";
import { enemyPool, enemyBombPool, alliedBombPool } from "./pools.js";
import {
    loadSounds,
    playSound,
    switchTrack, pauseTrack, resumeTrack,
    feedMusicPlayList, playNextPlaylist
} from "./sound.js";
import {
    speechSynthesisSupport,
    speak, stopSpeak, resumeSpeak
} from "./speechSynthesis.js";
import {
    historySupport,
    historyPush
} from "./history.js";
import { vibrationSupport, vibrate, vibrateForPermission } from "./vibration.js";
import { deathPattern, hurtPattern, bossPattern } from "./settings/vibrationPatterns.js";
import { initialGameState } from "./settings/gameState.js";
import { levels } from "./settings/levels.js";
import {
    getFast, getElseDefault, getElseSetDefault, setFast,
    save, setAndSave, clearAllData, setAndSaveSilent, explicitSave
} from "./localSave.js";
import {
    scoreUsed60Frames,
    scoreLoseLife, saveScore, setScore,
    scoreStartLevel
} from "./scores.js";
import { createSpawnGrids, useRandomSpawn, useSpawn, randomSpawn } from "./2DMath.js";
import { gamePadSupport, getGamePadInput, lastGamePadInput, gamePadMap } from "./gamepads.js";
import { lastClickAndTouchInput, clickAndTouchMap, resetClickAndTouch } from "./clickAndTouch.js";
import { lastKeyBoardInput, keyBoardMap, resetKeyBoard } from "./keyBoard.js";
import { serviceWorkerSupport, startServiceWorker, deleteServiceWorker } from "./service_worker/serviceWorkerManager.js";
import {
    listenForGamePadMenu, updateScoresMenu, updateLoadPossibility,
    throttledResumeOrPause, setGraphics, dispatchAction
} from "./ui.js";

export {
    spawnGrids, yMultiplier, xMultiplier,
    tickCount, onWantToUseGamePad,
    l, width, height, startLevel,
    startMultiplayer, startMultiplayerLevel,
    pause, resume, inGame, paused,
    resetGame, loadCheckPointTransition,
    quitTransition,
    defaultOptionsKeys, displayingText, gameState,
    allSounds
};


// -------- debugging --------

window.DEV = true;
window.d = d;
const l = function (...x) {
    console.log(...x);
};
// -------- debugging --------

/* safari 10 nomodule fix CC0-1.0
this snippet needs to be at the beginning of the
<script type="module" src="main-as-module.js"> and associated
<script nomodule src="main-as-script.js">
by throwing an error early the rest of the code is not executed
but only downloaded and compiled for Safari 10
assumption main-as-module.js and main-as-script.js
 are both built from the same source

advantage is that there is no js file that needs to be inlined
 at the top of the html

conclusion: worse for Safari10 better for everyone else

*/
if (window.MAIN_EXECUTED) {
    // "warning main.js already executed"
    throw new Error("warning main.js already executed");
}
window.MAIN_EXECUTED = true;



// --- game specifics --- (need to be in other files)
const grid = [`g1`, `g2`, `g3`, `g4`, `g5`, `g6`, `g7`, `g8`, `g9`];
const Y_MULTIPLIER_BALANCER = 1; // only change this value
let yMultiplier;
const BASE_HEIGHT = 320;
const X_MULTIPLIER_BALANCER = 0.8; //should be less than 1
let xMultiplier;
const BASE_WIDTH = 320;
const enemytopInitial = 20;
let spawnGrids;
let width;
let height;

const gameState = Object.assign({}, initialGameState);

// --- game engine ---
const targetFrameDuration = 1000 / 60;
let usingGamePad = false;
let paused = true;
let inGame = false; // not the same as paused
let tabClosing = false;
let frameCount;
let tickCount;
let oldTime;
let timeAccumulator;
let framesDropped;
let inputs = [];

const defaultOptions = {
    music: false,
    sound: false,
    speechSynthesis: false,
    vibrations: false,
    autoFullScreen: false,
    offline: false,
    graphics: `1`
};
const defaultOptionsKeys = Object.keys(defaultOptions);

// --- level manager ---
let levelId;
let levelProgression;
let currentDelay;
let currentDuration;
let currentAction;
let displayingText;
let actionPlaying;

// --- save load ---
let checkpointJustLoaded = false;
let lastSave = {
    lastCheckPointReached: false,
    // do not read if above is false
    lastCheckPointSaved: false,
    levelProgression: 0,
    levelId: 0,
    playerStateAtCheckPoint: {},
    width: BASE_WIDTH,
    height: BASE_HEIGHT
};


const onWantToUseGamePad = function () {
    usingGamePad = true;
    d.elements[d.scopeFromArray([`player1`, `controllerChoice`])].disabled = false;
    d.elements[d.scopeFromArray([`player2`, `controllerChoice`])].disabled = false;
    if (paused) {
        requestAnimationFrame(listenForGamePadMenu);
    }
};

window.onresize = function () {
    if (!inGame) {
        return;
    }
    if (!paused) {
        dispatchAction({ type: `back` });
    }

    const newHeight = innerHeight;
    const newWidth = innerWidth;

    if (newHeight !== height || newWidth !== width) {
        /*if a new size is detected, pause the game*/
        d.elements.pauseresizeexplanation.hidden = false;
        d.elements.resume.disabled = true;
        d.feed(`resolution`, `${width} x ${height}`);
        d.feed(`newresolution`, `${newWidth} x ${newHeight}`);
    } else {
        d.elements.pauseresizeexplanation.hidden = true;
        d.elements.resume.disabled = false;
    }
};


window.addEventListener(`beforeunload`, function () {
    tabClosing = true;
});

document.addEventListener(`visibilitychange`, function () {
    /* pause when switching away, but not when unloading
    https://stackoverflow.com/questions/46577684/ignore-visibilitychange-when-tab-is-being-closed/46577707#46577707*/
    if (document.hidden && !tabClosing && inGame) {
        dispatchAction({ type: `back` });
    }
});

//window.addEventListener(`unload`, function() {
//l(`3: unload`, document.hidden);
//});



d.functions.loadAutoSave = function () {
    resetGame();
    lastSave = getFast(`autoSave`);
    d.functions.loadCheckPoint();
};

const resetGameState = function () {
    Object.assign(gameState, initialGameState);
};

const startLevel = function (givenlevelId) {
    if (d.variables.vibrations) {
        // forces ask permissions
        vibrateForPermission();
    }
    resetGame();
    levelId = givenlevelId;
    scoreStartLevel();
    resume();
};

const startMultiplayerLevel = function ({ id, inputMode0, inputMode1 }) {
    if (d.variables.vibrations) {
        // forces ask permissions
        vibrateForPermission();
    }
    addPlayer();
    resetGame();
    levelId = id;
    scoreStartLevel();

    inputs[inputMode0] = Players[0];
    inputs[inputMode1] = Players[1];
    resume();
};

const addPlayer = function () {
    if (Players.length === 1) {
        let player2 = Player.create();
        player2.dom = d.elements.player2;
        player2.shield.dom = d.elements.shield2;
        Players.push(player2);
    }

    show(Players[1].dom);
};


d.functions.confirmInputMode = function () {
    const inputMode0 = d.variables[d.scopeFromArray([`player1`, `inputMode`])];
    const inputMode1 = d.variables[d.scopeFromArray([`player2`, `inputMode`])];

    if (inputMode0 === inputMode1) {
        d.feed(`inputModeError`, `Select Different input modes !`);
        return;
    }
    if (!(inputMode0) || !(inputMode1)) {
        d.feed(`inputModeError`, `Input modes must be selected !  `);
        return;
    }
    dispatchAction({ type: `startMultiplayerLevel`, id: `1`, inputMode0, inputMode1 });
};

const startMultiplayer = function () {
    show(d.elements.multipleInputsMenu);
    hide(d.elements.mainMenu);
};

const endLevel = function () {
    saveScore(levelId, `${width}X${height}`);
    updateScoresMenu();
};

const offerLastCheckPointReturn = function () {
    d.elements.loadCheckPoint.hidden = false;
    d.elements.resume.hidden = true;
};

const loadCheckPointTransition = function () {
    hide(d.elements.postMortemExplanation);
    d.elements.loadCheckPoint.hidden = true;
    d.elements.resume.hidden = false;

    // unpack
    ({ levelProgression, levelId } = lastSave);
    /* same as
    levelProgression = lastSave.levelProgression;
    levelId = lastSave.levelId;
    */
    currentAction = levels[levelId].steps[levelProgression];
    Player.assignForSave(player, lastSave.playerStateAtCheckPoint);

    // reset some stuff
    currentDuration = 0;
    currentDelay = 0;
    actionPlaying = false;

    cleanBoard();
    resume();
    checkpointJustLoaded = true;
};

d.functions.saveCheckPoint = function () {
    // we already have set, the correct playstate
    // from back then, only need to save (not silent)
    explicitSave().then(function (hasSaved) {
        if (hasSaved) {
            lastSave.lastCheckPointSaved = true;
            afterSaveCheckPoint();
        }
    });
};

const afterSaveCheckPoint = function () {
    d.elements.saveCheckPoint.disabled = true;
    d.feed(`saveCheckPoint`, `Checkpoint Saved !`);
};

const pause = function () {
    if (paused) {
        return;
    }
    paused = true;
    resetKeyBoard();
    resetClickAndTouch();
    if (usingGamePad) {
        requestAnimationFrame(listenForGamePadMenu);
    }
    if (lastSave.lastCheckPointReached && !lastSave.lastCheckPointSaved) {
        d.elements.saveCheckPoint.disabled = false;
    }
    d.elements.pauseMenu.hidden = false;
    d.elements.playground.hidden = true;
    d.elements.body.classList.add(`paused`);
    d.elements.pauseMenu.focus();
    pauseTrack();
    stopSpeak();
};

const resume = function () {
    if (!inGame || !paused) {
        return;
    }
    paused = false;

    hide(d.elements.postMortemExplanation);
    d.elements.pauseMenu.hidden = true;
    d.elements.mainMenu.hidden = true;
    d.elements.helpMenu.hidden = true;
    d.elements.loadMenu.hidden = true;
    d.elements.multipleInputsMenu.hidden = true;
    d.elements.playground.hidden = false;

    d.elements.body.classList.remove(`paused`);
    d.elements.controlGrid.focus();
    if (d.variables.music) {
        resumeTrack();
    }
    if (d.variables.speechSynthesis) {
        resumeSpeak();
    }

    const level1X = {
        name: "Level1",
        id: "1",
        steps: [
            {
                delayBefore: 0,
                blocking: true,
                action: "text",
                target: `Sergent Kai, 
            we registered an unauthenticated fleet approaching the scientific satellite Atomica.
            They are in direct violation of the law and must be eliminated before they reach their target.
            INCOMING !`,
                duration: 360
            },
            {
                delayBefore: 50,
                blocking: true,
                action: "spawn",
                target: "EnemySimple",
                count: 2
            },
            {
                delayBefore: 50,
                blocking: true,
                action: "text",
                target: `Care, the enemy has experimental weaponery !`,
                duration: 180
            },
            {
                delayBefore: 50,
                blocking: true,
                action: "spawn",
                target: "ExplosionLauncher",
                count: 1
            },
            {
                delayBefore: 50,
                blocking: true,
                action: "spawn",
                target: "EnemyDiagonale",
                count: 1
            },
            {
                delayBefore: 50,
                blocking: true,
                action: "text",
                target: `We don't have much time left !`,
                duration: 180
            },
            {
                delayBefore: 50,
                blocking: true,
                action: "spawn",
                target: "Rotator",
                count: 2
            },
            {
                delayBefore: 50,
                blocking: true,
                action: "spawn",
                target: "HardToCatch",
                count: 1,
                fixedCount: true
            },
            {
                delayBefore: 50,
                blocking: true,
                action: "spawn",
                target: "Dodger",
                count: 2
            },
            {
                delayBefore: 50,
                blocking: true,
                action: "spawn",
                target: "ConfusingShooter",
                count: 1
            },
            {
                delayBefore: 50,
                blocking: true,
                action: "spawn",
                target: "BossSwiper",
                count: 1
            },
            {
                delayBefore: 50,
                blocking: true,
                action: "spawn",
                target: "BossTargeter",
                count: 1
            },
            {
                delayBefore: 100,
                blocking: true,
                action: "text",
                target: "Big wave incoming ... arg ...",
                duration: 90
            },
            {
                delayBefore: 100,
                blocking: true,
                action: "spawn",
                target: "EnemySimple",
                count: 6
            },
            {
                delayBefore: 100,
                blocking: true,
                action: "spawn",
                target: "Dodger",
                count: 4
            },
            {
                delayBefore: 30,
                blocking: true,
                action: "spawn",
                target: "EnemySimple",
                count: 1
            },
            {
                delayBefore: 100,
                blocking: true,
                action: "text",
                target: "Sergent Kai, you must return immediately to the headquarters. Criminals attacked planet Ulkazir09Z during this small distraction",
                duration: 300
            },
            {
                delayBefore: 0,
                blocking: true,
                action: "text",
                target: "Victory !",
                duration: 200
            }
        ]
    };

    requestAnimationFrame(softStart);
    return true;
};

d.functions.resume = function () {
    dispatchAction(`resume`);
};


const softStart = function () {
    requestAnimationFrame(loop);
};

d.functions.restart = function () {
    resetGame();
    dispatchAction(`resume`);
};

d.functions.quitPauseMenu = function () {
    dispatchAction({ type: `back` });

};

const quitTransition = function () {

    show(d.elements.mainMenu);
    hide(d.elements.playground);
    hide(d.elements.pauseMenu);
    hide(d.elements.loadCheckPoint);
    inGame = false;
    updateLoadPossibility();

}



const actions = {
    // name, function that executes action
    "left": function (player) {

        player.x += -player.speedX;
        player.turn = -1;


    },
    "right": function (player) {

        player.x += player.speedX;
        player.turn = 1;

    },
    "swipeBoard": function (player) {
        /* bombA */

        if (player.nukes > 0 && !player.nukeActive) {
            player.nukeActive = true;
            player.nukes += -1;
            player.nukeEndFrame = player.nukeDuration + tickCount;
            d.elements.wipeboard.classList.add(`active`);
            setXPosition(d.elements.wipeboard, player.x + player.shootCenter);
            if (d.variables.sound) {
                playSound(d.elements.nukeSound)
            }

        }
    },
    "swipeLeft": function (player) {
        /* swipe left */
        if (player.swipes > 0 && !gameState.swipeLeftActive) {
            gameState.swipeLeftActive = true;
            player.swipes += -1;
            gameState.swipeLeftEndFrame = player.swipeDuration + tickCount;
            d.elements.leftswipe.classList.add(`active`);
        }
    },
    "swipeRight": function (player) {
        /* swipe right */
        if (player.swipes > 0 && !gameState.swipeRightActive) {
            gameState.swipeRightActive = true;
            player.swipes += -1;
            gameState.swipeRightEndFrame = player.swipeDuration + tickCount;
            d.elements.rightswipe.classList.add(`active`);
        }
    },
    "shield": function (player) {
        if ((player.shields > 0) &&
            ((!player.shieldActive) ||
                (player.shieldTimeRemaining < player.shieldOverlap))) {
            player.shield.dom.hidden = false;
            player.shield.dom.classList.remove(`urgence-shield`);
            player.shieldActive = true;
            player.shields += -1;
            player.shieldTimeRemaining = player.shieldDuration;
        }
    },
    "shielder": function (player) {
        if (player.shielderActive || player.shielders === 0) {
            return;
        }
        player.shielders += -1;
        player.shielderActive = true;
        player.shielderTimeRemaining = player.shielderDuration;
        player.shielderX = player.x;
        player.shielderXCenter = player.shielderX + player.shielderXHalfWidth;

        show(d.elements.shielder);
        d.elements.shielder.classList.add(`active`);
        setXPosition(d.elements.shielder, player.shielderX);
    },
    "slowTime": function (player) {
        if (gameState.slowTimeActive) {
            return;
        }
        if (player.slowTime === 0) {
            return;
        }
        player.slowTime += -1;
        gameState.slowTimeActive = true;
        gameState.slowedDivider = 2;
        gameState.slowTimeEndFrame = player.slowTimeDuration + tickCount;
    },
    "pause": function (player) {
        dispatchAction({ type: `back` });
    },
    "throttledResumeOrPause": throttledResumeOrPause
};

const playerFromInputId = function (inputId) {
    const playerAttachedToInput = inputs[inputId];
    if (!playerAttachedToInput) {
        return player;
    }
    return playerAttachedToInput;
};

const controllerId = `controller`;
const processGamePadInputs = function () {
    // l(lastGamePadInput);
    const controllerPlayer1 = lastGamePadInput.controllers[0];
    if (!controllerPlayer1) {
        return;
    }
    const playerAttachedToInput = playerFromInputId(controllerId);
    controllerPlayer1.buttons.forEach(function (button, i) {
        const {/*value, */pressed } = button;
        if (pressed && gamePadMap.hasOwnProperty(i)) {
            // console.log(i);
            // implicit to string conversion
            actions[gamePadMap[i]](playerAttachedToInput);
        }
    });

    /*for (let i = 0; i < controllerPlayer1.axes.length; i += 1) {*/

    const distance = controllerPlayer1.axes[0] * playerAttachedToInput.speedX;
    playerAttachedToInput.x += distance;
    if (distance > 0) {
        playerAttachedToInput.turn = 1;
    } else if (distance < 0) {
        playerAttachedToInput.turn = -1;
    }/* else if (distance === 0){
        // keep same direction playerAttachedToInput.turn
    }*/

};

const touchId = `touch`;
const processClickAndTouchInputs = function () {

    const playerAttachedToInput = playerFromInputId(touchId);
    Object.entries(lastClickAndTouchInput).forEach(function ([name, pressed]) {
        if (pressed) {
            actions[clickAndTouchMap[name]](playerAttachedToInput);
        }
    });
};

const keyBoardId = `keyboard`;
const processKeyboardsInputs = function () {
    const playerAttachedToInput = playerFromInputId(keyBoardId);
    Object.entries(lastKeyBoardInput).forEach(function ([keyCodeString, pressed]) {
        if (pressed) {
            actions[keyBoardMap[keyCodeString]](playerAttachedToInput);
        }
    });
};

const runLevelManager = function () {
    if (!currentAction) {
        levelProgression += 1;
        if (!(Object.prototype.hasOwnProperty.call(levels[levelId].steps, levelProgression))) {
            /*reached end*/
            pause(); // brings to the pause menu
            d.functions.quitPauseMenu(); // this bring to the main menu
            endLevel();
            return;
        }
        currentAction = levels[levelId].steps[levelProgression];
        currentDelay = currentAction.delayBefore;
        actionPlaying = false;
    } else if (currentDelay > 0) {
        currentDelay += -1;
    } else if (currentDuration > 0) {
        currentDuration += -1;
    } else if (currentDelay === 0 && !actionPlaying) {
        // arrive here after checkpoint has loaded
        actionPlaying = true;
        if (currentAction.checkpoint) {
            if (checkpointJustLoaded) {
                checkpointJustLoaded = false;
            } else {
                d.elements.notification.hidden = false;
                Player.assignForSave(lastSave.playerStateAtCheckPoint, player);

                lastSave.lastCheckPointReached = true;
                lastSave.levelProgression = levelProgression;
                lastSave.levelId = levelId;
                lastSave.width = width;
                lastSave.height = height;

                lastSave.lastCheckPointSaved = setAndSaveSilent(`autoSave`, lastSave);
                if (lastSave.lastCheckPointSaved) {
                    afterSaveCheckPoint();
                }
            }
        }
        if (Object.prototype.hasOwnProperty.call(currentAction, `classChange`) &&
            Array.isArray(currentAction.classChange)) {
            currentAction.classChange.forEach(function (name) {
                d.elements[name].classList.add(`highlight`);
            });
        }
        if (currentAction.playerSetter) {
            Players.forEach(function (player) {
                Object.assign(player, currentAction.playerSetter);
            });

        }
        if (currentAction.action === `text`) {
            if (d.variables.music) {
                switchTrack(d.elements.readingMusic);
            }
            if (d.variables.speechSynthesis) {
                speak(currentAction.target);
            }
            d.feed(`gametext`, currentAction.target);
            currentDuration = currentAction.duration;
            displayingText = true;
        } else if (currentAction.action === `spawn`) {
            if (d.variables.music) {
                playNextPlaylist(d.elements.actionSound);
            }
            const EnemyClass = Ships[currentAction.target];
            let howMany;
            if (currentAction.fixedCount) {
                howMany = currentAction.count;
            } else {
                howMany = currentAction.count * xMultiplier;
            }
            doNTimes(function () {
                let xPositon;
                if (EnemyClass.move) {
                    xPositon = randomSpawn(spawnGrids[EnemyClass.width]);
                } else {
                    // blocks spawn location
                    xPositon = useRandomSpawn(spawnGrids[EnemyClass.width]);
                }
                softActivate(
                    EnemyClass,
                    xPositon,
                    enemytopInitial
                );

            }, howMany);
        }
    } else if (enemyPool.getLength() === 0 && currentDuration === 0 && delayedPool.length === 0) {
        if (Object.prototype.hasOwnProperty.call(currentAction, `classChange`) &&
            Array.isArray(currentAction.classChange)) {
            currentAction.classChange.forEach(function (name) {
                d.elements[name].classList.remove(`highlight`);
            });
        }
        // this will trigger the next action on next frame
        currentAction = undefined;
        d.feed(`gametext`, ``);
        d.elements.notification.hidden = true;
        displayingText = false;
    }

};

const processInputs = function () {
    processGamePadInputs();
    processClickAndTouchInputs();
    processKeyboardsInputs();
};

const update = function () {
    /* tickCount is exported*/
    runLevelManager();
    processInputs();
    playerUpdate();

    if (gameState.slowTimeActive && tickCount % 2 === 0) {
        // slowed = true
        gameState.slowedIncrement = 0;
    } else {
        gameState.slowedIncrement = 1;
        enemiesUpdate();
    }
    bombsUpdate();
    /* slowed with slowedIncrement, slowedDivider for smoothness*/
    if (player.life <= 0) {
        if (currentAction === undefined) {
            /*finished at the same frame continue*/
            player.life = 1;
        } else {
            /* the player reached 0 life, game ends*/
            dispatchAction({ type: `back`, reason: `death` });
            show(d.elements.postMortemExplanation);
            if (lastSave.lastCheckPointReached) {
                offerLastCheckPointReturn();
                d.feed(`checkPointHint`, `A checkpoint was found !`);
            } else {
                d.feed(`checkPointHint`, `No checkpoint was found`);
            }
        }
    }
    if (tickCount % 60 === 0) {
        scoreUsed60Frames();
        lateActivate();
    }
};

const render = function () {
    d.feed(`topbar`, `F-Dropped: ${framesDropped}, LIFE: ${player.life}, SHIELD: ${player.shields}, SWIPE: ${player.swipes}, NUKE: ${player.nukes}, SLOW: ${player.slowTime}, SHIELDER: ${player.shielders}`);

    Players.forEach(function (player) {

        setXPosition(player.dom, player.x);
    });

    enemyBombPool.forEach(function (enemyBomb) {
        setYPosition(enemyBomb.dom, enemyBomb.y);
        setXPosition(enemyBomb.dom, enemyBomb.x);
    });

    alliedBombPool.forEach(function (bomb) {
        setYPosition(bomb.dom, bomb.y);
    });
};

const frameCounter = function (NowTime) {
    if (frameCount % 60 === 0) {
        scoreUsed60Frames();
        framesDropped = tickCount - frameCount;
    }
};

const loop = function (NowTime) {
    // could use cancelRequestAnimationFrame instead for optimal perf
    if (paused) {
        return;
    }
    // do requestAnimationFrame as soon as possible for perfect timing
    requestAnimationFrame(loop);
    frameCounter(NowTime);
    const timeDelta = NowTime - oldTime;
    oldTime = NowTime;
    // timeAccumulator += timeDelta;
    // in the meantime of chosing fps
    // safer if computer is really slow
    timeAccumulator = (timeAccumulator + timeDelta) % 500;
    getGamePadInput();
    while (timeAccumulator >= targetFrameDuration) {
        timeAccumulator -= targetFrameDuration;
        update();
        tickCount += 1;
    }
    // l(timeDelta);
    render();
    frameCount += 1;
};

const initialize = function () {
    // engine
    inGame = true;
    oldTime = 0;
    frameCount = 0;
    tickCount = 0;
    timeAccumulator = 0;

    // level manager
    levelProgression = -1;
    currentAction = undefined;
    currentDelay = 0;
    currentDuration = 0;
    displayingText = false;
    actionPlaying = false;

    // save
    lastSave.lastCheckPointReached = false;
    lastSave.lastCheckPointSaved = false;

    // game logic
    width = innerWidth;
    height = innerHeight;
    yMultiplier = 1 + ((Y_MULTIPLIER_BALANCER * (height - BASE_HEIGHT)) / BASE_HEIGHT);
    xMultiplier = 1 + ((X_MULTIPLIER_BALANCER * (width - BASE_WIDTH)) / BASE_WIDTH);
    spawnGrids = createSpawnGrids(xMultiplier, width);
    resetAllBodies();
    resetGameState();
};

const cleanDom = function () {
    d.elements.pauseresizeexplanation.hidden = true;
    d.elements.resume.disabled = false;
    d.elements.resume.hidden = false;
    d.elements.shielder.classList.remove(`active`);
    d.elements.shielderlaser.hidden = true;
    d.elements.leftswipe.classList.remove(`active`);
    d.elements.rightswipe.classList.remove(`active`);
    d.elements.wipeboard.classList.remove(`active`);
    Players.forEach(function (player) {
        player.dom.classList.remove(`hurt`);
        player.shield.dom.hidden = true;
        player.shield.dom.classList.remove(`urgence-shield`);
    });

    grid.forEach(function (name) {
        d.elements[name].classList.remove(`highlight`);
    });
};

const resetGame = function () {
    if (d.variables.autoFullScreen) {
        goFullScreen();
        // do not use toggle as it could exit also
    }
    // hide other multiplayers from before
    Players.forEach(function (player, i) {
        if (i > 0) {
            hide(player.dom);
        }
    });
    Players.length = 1;

    inputs = [];
    cleanBoard();
    initialize();
};

const cleanBoard = function () {
    cleanDom();
    wipeBoard();
};

let allSounds;
useYesNoDialog(d);
d.start(
    document.body, // start Element
    {
        ...defaultOptions,
        ...getElseDefault(`options`, {}),
        "player1": {
            inputMode: getElseDefault(`player1`, { inputMode: `` }).inputMode
        },
        "player2": {
            inputMode: getElseDefault(`player2`, { inputMode: `` }).inputMode
        }
    }, //initial feed
    {}, // functions
    function () {
        player.dom = d.elements.player;
        player.shield.dom = d.elements.shield;
        allSounds = [d.elements.nukeSound];
        feedMusicPlayList([
            d.elements.actionSound0,
            d.elements.actionSound1,
            d.elements.actionSound2,
            d.elements.actionSound3
        ]);
        updateLoadPossibility();
        updateScoresMenu();
        if (!fscreen.fullscreenEnabled) {
            d.elements.fullScreen1.hidden = true;
            d.elements.fullScreen2.hidden = true;
        }
        setGraphics(d.variables.graphics);

        if (d.variables.sound) {
            loadSounds(allSounds);
        }

        if (
            window.navigator &&
            (/Safari/).test(window.navigator.userAgent) &&
            !window.SharedWorker
        ) {
            d.elements.safariWarning.hidden = false;
        }

        if (!serviceWorkerSupport) {
            d.elements.offline.hidden = true;
        }

        if (!vibrationSupport) {
            d.elements.vibration.hidden = true;
        } else {
            // force ask prompt
            if (d.variables.vibrations) {
                // forces ask permissions
                vibrateForPermission();
            }
        }

        if (!speechSynthesisSupport) {
            d.elements.speechSynthesis.hidden = true;
        }

        d.elements.loadingHint.remove();
    });
