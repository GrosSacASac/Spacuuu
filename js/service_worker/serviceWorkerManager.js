/*jslint
    maxerr: 100, browser, devel, fudge, maxlen: 120, white
*/
/*global
    window, navigator
*/

export {
    serviceWorkerSupport,
    startServiceWorker,
    deleteServiceWorker
};

// has to be in sync with scope
const serviceWorkerPath = "/service_worker.min.js";

// true means maybe
// sw are disabled on private browsing
const serviceWorkerSupport = (
    window.navigator &&
    window.navigator.serviceWorker &&
    window.navigator.serviceWorker.register
);

let serviceWorkerRegistration;

const startServiceWorker = function (updateListener) {
    if (!serviceWorkerSupport) {
        return Promise.reject('No service worker support');
    }
	let count = 0;
	return new Promise(function (resolve, reject) {
		navigator.serviceWorker.register(serviceWorkerPath).then(function (registrationObject) {
			serviceWorkerRegistration = registrationObject;
			console.log("service worker registration success!: serviceWorkerRegistration", serviceWorkerRegistration);
			console.log("navigator.serviceWorker: ", navigator.serviceWorker);
			
			updateListener(0, 1); // 0%
			navigator.serviceWorker.addEventListener("message", function (event) {
				const {data} = event;
                const {intent} = data;
                if (intent === `update`) {
                    const {total, added, failed} = data;
                    if (added) {
                        count += 1;
                        console.log(`added ${added} ${count} / ${total}`);
                        updateListener(count, total);
                    } else if (failed) {
                        const {reason} = data;
                        // const errorString = `error ${failed} reason: ${reason}`;
                        registrationObject.unregister();
                        console.error(reason);
                        reject(reason);
                    }
                }
			});			

		}).catch(function(reason) {
			console.error("service worker could not register:", reason);
			reject(reason);
		});


		navigator.serviceWorker.addEventListener("activate", function (event) {
			console.log("serviceWorker: activate", location.origin, event);
			
		});

		navigator.serviceWorker.addEventListener("controllerchange", function (event) {
			console.log("serviceWorker: controllerchange", event);
            resolve();
		});
	});
};

const unregisterOne = function (registration) {
	if (!registration) {
		return;
	}
	registration.unregister().then(function(event) {
		console.log("serviceWorker: unregistered", event);
	});
};

const deleteServiceWorker = function () {
    if (!serviceWorkerSupport) {
        return;
    }	
	
    if (navigator.serviceWorker.getRegistrations) {
        navigator.serviceWorker.getRegistrations().then(function(registrations) {
            for (let registration of registrations) {
                unregisterOne(registration);
            }
        });
    } else if (navigator.serviceWorker.getRegistration) {
        // not plural
        navigator.serviceWorker.getRegistration().then(function(registration) {
			unregisterOne(registration);
        });
    }

    // this could fail to remove something not yet active
    // serviceWorkerRegistration.unregister().then(function(event) {
        // console.log("serviceWorker: unregistered", event);
    // });
};
