import * as d from "../node_modules/dom99/built/dom99ES.js";
import {vibrate} from "./vibration.js";
import {deathPattern, hurtPattern, bossPattern} from "./settings/vibrationPatterns.js";
import {playSound} from "./sound.js";
import {setXPosition, setYPosition, setDegreeAngle, hide, show } from "./dom.js";
import {insideAxis, clampValue } from "./2DMath.js";
import {enemyPool, enemyBombPool, alliedBombPool} from "./pools.js";
import {scoreLoseLife, scoreKill} from "./scores.js";
import {yMultiplier, xMultiplier, tickCount, height, width, displayingText, l, gameState} from "./main.js";
// could combine  yMultiplier * and player.slowMultiplier
import {unitsWithBaseSpeed, player, Player, Players} from "./settings/units.js";
export {
    enemiesUpdate, bombsUpdate, resetAllBodies, playerUpdate, wipeBoard, softActivate, lateActivate, delayedPool, isNewPositionInConflictEnemyPool
};

let halfWidth;

const isNewPositionInConflictEnemyPool = function (width, height, newX, newY, instance) {
    return enemyPool.some(function (enemy) {
        if (enemy === instance) {
            // comparing with self, skip
            return false;
        }
        if (
            insideAxis(newX, width, enemy.x, enemy.is.width) &&
            insideAxis(newY, height, enemy.y, enemy.is.height)
        ) {
            // console.log("colliding");
            return true;
        }
        return false;
    });
};

let delayedPool = [];
const softActivate = function (Class, x, y) {
    // returns true if it was indeed activated
    if (isNewPositionInConflictEnemyPool(Class.width, Class.height, x, y, undefined)) {
        delayedPool.push({
            Class,
            x,
            y
        });
        return false;
    } else {
        enemyPool.activate(
            Class,
            x,
            y
        );
        return true;
   }
};

const lateActivate = function () {
    // tries to activate delayed things
    // recursive

    if (delayedPool.length > 0) {
        const specs = delayedPool[delayedPool.length - 1];
        const {
            Class,
            x,
            y
        } = specs;
        if (!isNewPositionInConflictEnemyPool(Class.width, Class.height, x, y, undefined)) {
            enemyPool.activate(
                Class,
                x,
                y
            );
            delayedPool.pop();
            lateActivate();
        }


    }
};

const resetAllBodies = function () {
    /* called just after width, height, yMultiplier etc are reset*/
  delayedPool = [];
  halfWidth = (width / 2) - 5;// -5 to account for width offset
  wipeBoard();
  
  Players.forEach(function (player) {
    Player.reset(player);
    Object.assign(player.weapon, {
        bombInitialY: player.y - player.weapon.height + 1,
    });
    setYPosition(player.dom, player.y);
  });

  unitsWithBaseSpeed.concat(Players).forEach(function (unit) {
    if (unit.hasOwnProperty(`baseSpeedX`)) {
        unit.speedX = unit.baseSpeedX * xMultiplier;
    }
    if (unit.hasOwnProperty(`baseSpeedY`)) {
        unit.speedY = unit.baseSpeedY * yMultiplier;
    }

    // if (!unit.hasOwnProperty(`baseSpeedX`) &&
    //     !unit.hasOwnProperty(`baseSpeedY`)) {
    //     console.warn(unit.id, `has no baseSpeed at all !`)
    // }

  });
};

const bombsUpdate = function () {
    alliedBombPool.forEach(function (bomb) {
        const timedout = ((bomb.birthDate + bomb.is.maxAge) < tickCount);
        const collided = colision(bomb, enemyPool);
        const out = (bomb.y + bomb.is.height) < 0;
        if (timedout || collided || out) {
            alliedBombPool.deActivate(bomb);
            return;
        }

        bomb.y += -bomb.is.speedY;
    });

    enemyBombPool.forEach(function (enemyBomb) {
        const timedout = ((enemyBomb.birthDate + enemyBomb.is.maxAge) < tickCount);
        const collided = colision(enemyBomb, Players);
        const out = enemyBomb.y > height;
        if (timedout || collided || out) {
            enemyBombPool.deActivate(enemyBomb);
            return;
        }
        if (enemyBomb.is.move) {
            enemyBomb.is.move(enemyBomb);
        } else {
            enemyBomb.y += enemyBomb.is.speedY / gameState.slowedDivider;
        }
    });
};

const colision = function (bomb, targets) {
    const BombClass = bomb.is;
    const bx = bomb.x;
    const by = bomb.y;
    const hit = targets.some(function (target, index) {
        const TargetClass = target.is;
        if (insideAxis(bx, BombClass.width, target.x, TargetClass.width) &&
            insideAxis(by, BombClass.height, target.y, TargetClass.height)) {
            if (target.is === Player && target.shieldActive) {
                Player; // do something ?
            } else {
                target.life += -1;
                if (target.life === 0) {
                    if (target.is === Player) {
                        if (d.variables.vibrations) {
                            vibrate(deathPattern);
                        }
                        // logic is in the main loop
                    } else {
                      enemyPool.deActivate(target);
                    }
                } else {
                    target.dom.classList.add("hurt");
                    if (target.is === Player) {
                        if (d.variables.vibrations) {
                            vibrate(hurtPattern);
                        }
                        player.shieldActive = true;
                        player.shieldTimeRemaining =  player.survivalShieldDuration;
                        player.shield.dom.hidden = false;
                        player.shield.dom.classList.add(`urgence-shield`);
                        scoreLoseLife();
                    }
                }
            }
            return true;
        }
    });
    return hit;
};

const enemiesUpdate = function () {
    enemyPool.forEach(function (enemy) {
      enemy.is.shoot(enemy);
      if (enemy.is.move) {
        enemy.is.move(enemy);
      }
    });
};

const isLeft = function (b) {
    return (b.x < halfWidth);
};

const filterHalf = function () {
    enemyPool.forEach(function (instance) {
        if (isLeft(instance)) {
            enemyPool.deActivate(instance);
        }
    });
    enemyBombPool.forEach(function (instance) {
        if (isLeft(instance)) {
            enemyBombPool.deActivate(instance);
        }
    });
};

const filterRightHalf = function (object) {
    enemyPool.forEach(function (instance) {
        if (!isLeft(instance)) {
            enemyPool.deActivate(instance);
        }
    });
    enemyBombPool.forEach(function (instance) {
        if (!isLeft(instance)) {
            enemyBombPool.deActivate(instance);
        }
    });
};

const applySwipeEffect = function () {
    if (gameState.swipeLeftActive) {
        filterHalf();
    }
    if (gameState.swipeRightActive) {
        filterRightHalf();
    }
};

const wipeBoard = function () {
    enemyPool.forEach(enemyPool.deActivate);
    enemyBombPool.forEach(enemyBombPool.deActivate);
    alliedBombPool.forEach(alliedBombPool.deActivate);
};

const applyNukeEffect = function () {
  /* remove everything at the beginning and end
   from a gameplay perspective it is the same*/
    if (player.nukeActive) {
      if (player.nukeEndFrame - tickCount === player.nukeInvertedDelay ||
        player.nukeEndFrame ===  tickCount) {
        // almost the same as doing it continuosly
        wipeBoard();
      }
    }
};

const consumeNuke = function () {
    if (player.nukeActive && player.nukeEndFrame === tickCount) {
        player.nukeActive = false;
        d.elements.wipeboard.classList.remove("active");
    }
};

const applyShielderEffect = function () {
    if (player.shielderActive &&
        enemyBombPool.getLength() > 0 &&
        player.shielderInvertedDelay > player.shielderTimeRemaining) {
        /*get the closes point and shoot it
        */

        let bomb;
        if (player.shielderLastTarget && enemyBombPool.includes(player.shielderLastTarget)) {
          bomb = player.shielderLastTarget;
        } else {
          bomb = enemyBombPool.map(function (bomb) {
              const relativeX = bomb.x - player.shielderXCenter;
              const relativeY = bomb.y - player.shielderYCenter;
              // Not real distance, but still sorted
              // because omitting the square root does not
              // change order
              const hierarchicalDistance = (relativeX ** 2) + (relativeY **2);
              return {
                  hierarchicalDistance,
                  bomb
              };
          }).sort(function (bombA, bombB) {
              return bombA.hierarchicalDistance - bombB.hierarchicalDistance;
          })[0].bomb;
        }

        // offset to target the center
        const xOffSet = bomb.is.width / 2;
        const yOffSet = bomb.is.height / 2;
        const degreeAngle = Math.atan2(
          (bomb.y + yOffSet) - player.shielderYCenter, (bomb.x + xOffSet) - player.shielderXCenter
        ) * (180 / Math.PI);
        const distance = (
          ((bomb.x + xOffSet - player.shielderXCenter) ** 2) +
          ((bomb.y + yOffSet - player.shielderYCenter) ** 2)) ** 0.5;
        bomb.life += -1;
        if (bomb.life === 0) {
          enemyBombPool.deActivate(bomb);
          player.shielderLastTarget = undefined;
        } else {
          player.shielderLastTarget = bomb;
          bomb.dom.classList.add("targeted");
          d.elements.shielderlaser.hidden = false;
          setDegreeAngle(d.elements.shielderlaser, degreeAngle);
          d.elements.shielderlaser.style.width = `${distance}px`;
        }
    } else if (player.shielderActive) {
      d.elements.shielderlaser.hidden = true;
    }
};

const consumeShielder = function () {
    if (player.shielderActive) {
        player.shielderTimeRemaining += -1;
        if (player.shielderTimeRemaining === 0) {
            player.shielderActive = false;
            hide(d.elements.shielder);
            hide(d.elements.shielderlaser);
            d.elements.shielder.classList.remove("active");
            if (player.shielderLastTarget && enemyBombPool.includes(player.shielderLastTarget)) {
              player.shielderLastTarget.dom.classList.remove("targeted");
            }
        }
    }
};

const consumeSwipes = function () {
    if (gameState.swipeLeftActive && gameState.swipeLeftEndFrame === tickCount) {
        gameState.swipeLeftActive = false;
        d.elements.leftswipe.classList.remove("active");
    }
    if (gameState.swipeRightActive && gameState.swipeRightEndFrame === tickCount) {
        gameState.swipeRightActive = false;
        d.elements.rightswipe.classList.remove("active");
    }
};

const consumeSlowTime = function () {
    if (gameState.slowTimeActive && gameState.slowTimeEndFrame === tickCount) {
        gameState.slowTimeActive = false;
        gameState.slowedDivider = 1;
    }
};

const consumeShield = function () {
    Players.forEach(function (player) {
        if (player.shieldActive) {
            player.shieldTimeRemaining += -1;
            if (player.shieldTimeRemaining === 0) {
                player.shieldActive = false;
                player.shield.dom.hidden = true;
            }
        }
    });
};

const movePlayer = function () {
    /* use this before all other in game events to have the latest position
    but before all movements input for proper capping (min, max)*/
    Players.forEach(function (player) {
      player.x = clampValue(
            player.x + player.turn,
            /*min=*/0,
            /*max=*/player.maxX
      );
    });
};

const playerUpdate = function () {

    movePlayer();
    if (!displayingText && !player.nukeActive) {
      Players.forEach(function (player) {
        Player.playerShoot(player);
      });
    }
    applySwipeEffect();
    consumeSwipes();

    applyNukeEffect();
    consumeNuke();

    applyShielderEffect();
    consumeShielder();

    consumeSlowTime();

    consumeShield();
};
