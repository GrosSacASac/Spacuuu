/* https://developer.mozilla.org/en-US/docs/Web/API/SpeechSynthesis

*/

export {
    speechSynthesisSupport, speak, stopSpeak, resumeSpeak
};


const speechSynthesisSupport = (
    window.SpeechSynthesisUtterance &&
    window.speechSynthesis
);

let speak;
let stopSpeak;
let resumeSpeak;
if (speechSynthesisSupport) {
    // https://developer.mozilla.org/en-US/docs/Web/API/SpeechSynthesis
    // const voices = speechSynthesis.getVoices();
    let utterance;
    speak = function (text) {
        utterance = new SpeechSynthesisUtterance();
        utterance.lang = "en";
        utterance.text = text;
        /*utterance.voice = voices[5]; // Note: some voices don't support altering params
        utterance.voiceURI = 'native';
        utterance.volume = 1; // 0 to 1
        utterance.rate = 1; // 0.1 to 10
        utterance.pitch = 2; //0 to 2*/
        speechSynthesis.speak(utterance);
        //if (speechSynthesis.paused && speechSynthesis.speaking) {
            // pause the global not the instance
            speechSynthesis.resume();
        //}
        
    };
    stopSpeak = function (text) {

        //if (!speechSynthesis.paused && speechSynthesis.speaking) {
            // pause the global not the instance
            speechSynthesis.pause();
        //}
    };
    resumeSpeak = function () {
        speechSynthesis.resume();
    };
} else {
    const empty = function () {};
    speak = empty;
    stopSpeak = empty;
    resumeSpeak = empty;
}

