/*

*/

export {lastKeyBoardInput, keyBoardMap, resetKeyBoard};
import {keyBoardMap} from "./settings/keyBoardMap.js"

import * as d from "../node_modules/dom99/built/dom99ES.js"; 


// true means pressed, false means not pressed
const lastKeyBoardInput = {};
// only add keys that are used
Object.keys(keyBoardMap).forEach(function (keyCodeString) {
    // initially not pressed
    lastKeyBoardInput[keyCodeString] = false;
});

const resetKeyBoard = function () {
    Object.keys(lastKeyBoardInput).forEach(function (keyCodeString) {
        // initially not pressed
        lastKeyBoardInput[keyCodeString] = false;
    });
};

d.functions.keydown = function (event) {
    // console.log(event.keyCode);
    const keyCode = event.keyCode;
    const keyCodeString = String(keyCode);
    if (lastKeyBoardInput.hasOwnProperty(keyCodeString)) {
        lastKeyBoardInput[keyCodeString] = true;
    }
};

d.functions.keyup = function (event) {
    // console.log(event.keyCode);
    const keyCode = event.keyCode;
    const keyCodeString = String(keyCode);
    if (lastKeyBoardInput.hasOwnProperty(keyCodeString)) {
        lastKeyBoardInput[keyCodeString] = false;
    }
};
