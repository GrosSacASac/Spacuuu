/*
https://dev.opera.com/articles/html5-history-api/
*/
export {
    historySupport,
    historyPush,
    historyBack,
    historyReplace,
    historyBackAndReplace
};
import {restoreHistory} from "./ui.js";

const historySupport = (
    window.history &&
    window.History && // constructor
    [
        "state",
        "go",
        "back",
        "forward",
        "pushState",
        "replaceState",
    ].every(function (historyFeature) {
        return historyFeature in window.history;
    })
);

let historyPush;
let historyBack;
let historyReplace;
let historyBackAndReplace;
if (historySupport) {
    let ignoreNext = false;
    let nextData;
    historyReplace = function (data) {
        //console.log("history replace", data);
        history.replaceState(data, null, null);
    };
    historyBackAndReplace = function (data) {
        console.log("history historyBackAndReplace", data);
        ignoreNext = true;
        history.back();
        nextData = data;
        setTimeout(function () {
            ignoreNext = false;
        }, 1);
    };
    historyPush = function (data) {
       // console.log("history push", data);
        history.pushState(data, null, null);
    };
    historyBack = function () {
        console.log("history back");
        history.back();
    };
    window.addEventListener("popstate", function(event){
        if (ignoreNext) {
            ignoreNext = false;
            historyReplace(nextData);
            nextData = undefined;
            return;
        }
        console.log(history.state);
        if (history.state) {
            //console.log(e.state.path, );
        }
        // force default parameter of function
        restoreHistory(history.state || undefined);
    }, false);
} else {
    historyPush = function () {};
    historyBack = historyPush;
    historyReplace = historyPush;
    historyBackAndReplace = historyPush;
}
