/*
g1 - 9 is the grid where 1 is top left and 9 is bottom right 
d = down
u = up
it is necessary to up virtually up neighbour keys everytime a key is pressed
because you can sometimes press somewhere slide over time, and up somewhere else
    otherwise that would stay infinitely pressed

reset everything on pause, just to for safety, so user has a chance to reset stuff if any
    assumptions are wrong

https://stackoverflow.com/questions/7018919/how-to-bind-touchstart-and-click-events-but-not-respond-to-both

here we don't care because we do static variable = false || true
so firing event once or twice does not change anything
*/
export {clickAndTouchMap} from "./settings/clickAndTouchMap.js";
export {lastClickAndTouchInput, resetClickAndTouch};

import * as d from "../node_modules/dom99/built/dom99ES.js"; 


// true means pressed
const lastClickAndTouchInput = {
    g1: false,
    g2: false,
    g3: false,
    g4: false,
    g5: false,
    g6: false,
    g7: false,
    g8: false,
    g9: false,
};

const resetClickAndTouch = function () {
    Object.keys(lastClickAndTouchInput).forEach(function (name) {
        // initially not pressed
        lastClickAndTouchInput[name] = false;
    });
};

d.functions.g1d = function () {
    lastClickAndTouchInput.g1 = true;
};
d.functions.g1u = function () {
    lastClickAndTouchInput.g1 = false;
    lastClickAndTouchInput.g2 = false;
    lastClickAndTouchInput.g4 = false;
    lastClickAndTouchInput.g5 = false;
};

d.functions.g2d = function () {
    lastClickAndTouchInput.g2 = true;
};
d.functions.g2u = function () {
    lastClickAndTouchInput.g2 = false;
    lastClickAndTouchInput.g1 = false;
    lastClickAndTouchInput.g3 = false;
    lastClickAndTouchInput.g4 = false;
    lastClickAndTouchInput.g5 = false;
    lastClickAndTouchInput.g6 = false;
};

d.functions.g3d = function () {
    lastClickAndTouchInput.g3 = true;
};
d.functions.g3u = function () {
    lastClickAndTouchInput.g3 = false;
    lastClickAndTouchInput.g2 = false;
    lastClickAndTouchInput.g5 = false;
    lastClickAndTouchInput.g6 = false;
};

d.functions.g4d = function () {
    lastClickAndTouchInput.g4 = true;
};
d.functions.g4u = function () {
    lastClickAndTouchInput.g4 = false;
    lastClickAndTouchInput.g1 = false;
    lastClickAndTouchInput.g2 = false;
    lastClickAndTouchInput.g5 = false;
    lastClickAndTouchInput.g7 = false;
    lastClickAndTouchInput.g8 = false;
};

d.functions.g5d = function () {
    lastClickAndTouchInput.g5 = true;
};
d.functions.g5u = function () {
    // disables all because center of grid
    lastClickAndTouchInput.g1 = false;
    lastClickAndTouchInput.g2 = false;
    lastClickAndTouchInput.g3 = false;
    lastClickAndTouchInput.g4 = false;
    lastClickAndTouchInput.g5 = false;
    lastClickAndTouchInput.g6 = false;
    lastClickAndTouchInput.g7 = false;
    lastClickAndTouchInput.g8 = false;
    lastClickAndTouchInput.g9 = false;
};

d.functions.g6d = function () {
    lastClickAndTouchInput.g6 = true;
};
d.functions.g6u = function () {
    lastClickAndTouchInput.g6 = false;
    lastClickAndTouchInput.g2 = false;
    lastClickAndTouchInput.g3 = false;
    lastClickAndTouchInput.g5 = false;
    lastClickAndTouchInput.g8 = false;
    lastClickAndTouchInput.g9 = false;
};

d.functions.g7d = function () {
    // also disables go right
    lastClickAndTouchInput.g9 = false;
    lastClickAndTouchInput.g7 = true;
};
d.functions.g7u = function () {
    lastClickAndTouchInput.g7 = false;
    lastClickAndTouchInput.g4 = false;
    lastClickAndTouchInput.g5 = false;
    lastClickAndTouchInput.g8 = false;
};

d.functions.g8d = function () {
    lastClickAndTouchInput.g8 = true;
};
d.functions.g8u = function () {
    lastClickAndTouchInput.g8 = false;
    lastClickAndTouchInput.g4 = false;
    lastClickAndTouchInput.g5 = false;
    lastClickAndTouchInput.g6 = false;
    lastClickAndTouchInput.g7 = false;
    lastClickAndTouchInput.g9 = false;
};

d.functions.g9d = function () {
    // also disables go left
    lastClickAndTouchInput.g7 = false;
    lastClickAndTouchInput.g9 = true;
};
d.functions.g9u = function () {
    lastClickAndTouchInput.g9 = false;
    lastClickAndTouchInput.g5 = false;
    lastClickAndTouchInput.g6 = false;
    lastClickAndTouchInput.g8 = false;
};

