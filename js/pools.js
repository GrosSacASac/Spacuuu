import {addDom, setXPosition, setYPosition} from "./dom.js";
export {enemyPool, enemyBombPool, alliedBombPool};

const createPoolManager = function () {
    const pools = {};
    const activate = function (ObjectClass, ...rest) {
        if (!Object.prototype.hasOwnProperty.call(pools, ObjectClass.id)) {
            pools[ObjectClass.id] = [];
        }
        const pool = pools[ObjectClass.id];
        const notActive = pool.find(function (instance) {
            return !instance.active;
        });
        let instance;
        if (notActive === undefined) {
            instance = ObjectClass.create(...rest);
            addDom(instance.dom);
            pool.push(instance);
        } else {
            instance = notActive;
            ObjectClass.cleanUp(instance);
            ObjectClass.configure(instance, ...rest);
            instance.dom.hidden = false;
        }
        setXPosition(instance.dom, instance.x);
        setYPosition(instance.dom, instance.y);
        instance.active = true;
        return instance;
    };
    
    const deActivate = function (instance) {
        if (instance.is.beforeDeActivated !== undefined) {
            instance.is.beforeDeActivated(instance);
        }
        instance.dom.hidden = true;
        instance.active = false;
    };
    
    const forEach = function (callBack) {
        // could be faster
        /* by putting*/
        Object.values(pools).forEach(function (pool) {
            pool.forEach(function (instance) {
                if (!instance.active) {
                    return;
                }
                callBack(instance);
            });
        });
    };    
    
    const some = function (callback) {
        // could be faster
        /* by putting*/
        return Object.values(pools).some(function (pool) {
            return pool.some(function (instance) {
                if (!instance.active) {
                    return false;
                }
                return callback(instance);
            });
        });
    };
    
    const includes = function (what) {
        // could be faster
        /* by putting*/
        return some(function (instance) {
            return instance === what;
        });
    };
    
    const map = function (callback) {
        // could be faster
        /* by putting*/
        const mapped = []
        forEach(function (instance) {
            mapped.push(callback(instance));
        });
        return mapped;
    };
    
    const getLength = function () {
        let count = 0;
        forEach(function () {
            count += 1;
        });
        return count;
    };
    
    return {
        forEach,
        activate,
        deActivate,
        getLength,
        includes,
        some,
        map
    };
};

const enemyPool = createPoolManager();
const enemyBombPool = createPoolManager();
const alliedBombPool = createPoolManager();

// usage 
/*
const enemyInstance = enemyPool.activate(EnemyClass, x, y, z);
enemyPool.deActivate(enemyInstance);
enemyPool.forEach(...)

the manager is adding, hiding and removing from the dom

where EnemyClass has at least 
    * create
    * beforeDeActivate
    * resetFast(instance);
    * configure(...rest);
    * cleanUp (before reactivate)
    
and enemyInstance 
    * dom
    * active // not used 

*/
