/*
use softExtend as a decorator around the class
*/

export {softExtend};

const softExtend = function (BaseClass, ChildClass) {
    Object.entries(BaseClass).forEach(function ([key, value]) {
        // use in operator to not overwrite explicit undefined
        // and make it work with already subclasses
        if (!(key in ChildClass)) {
            // console.log(key, "not in", ChildClass);
            // console.log("using default");
            ChildClass[key] = value;
        
        }
    });
    return ChildClass;
};

