/* https://developer.mozilla.org/en-US/docs/Web/API/SpeechSynthesis

here we distinguish sounds and musics
*/

export {
    feedMusicPlayList, playNextPlaylist,
    switchTrack, playSound, loadSounds, pauseTrack, resumeTrack
};


let track;
const playList = [];
let nextMusicIndex = 0;

const feedMusicPlayList = function (musics) {
    playList.push(...musics);
};

const playNextPlaylist = function () {
    switchTrack(playList[nextMusicIndex]);
    nextMusicIndex = (nextMusicIndex + 1) % playList.length;
};


const switchTrack = function (newTrack) {
  /* only 1 track can be played at the same time*/
  if (newTrack && newTrack !== track) {
    if (track) {
      track.pause();
    }
    track = newTrack;
  }
  track.play();
};

const pauseTrack = function () {
  if (!track) {
    return;
  }
  track.pause();
};

const resumeTrack = function () {
  if (!track) {
    return;
  }
  track.play();
};

const playSound = function (sound) {
  /* only plays ready sound to avoid delays between fetching*/
  if (sound.readyState < sound.HAVE_ENOUGH_DATA) {
    return;
  } else {
    // make sure it is loaded for next time
    sound.load();
  }
  sound.play();
};

/*assumes one group of sounds to laod*/
let soundsLoaded = false;
const loadSounds = function (sounds) {
    if (soundsLoaded) {
        return;
    }
    sounds.forEach(function (sound) {
      try {
        sound.preload = "auto"; // is enough to start loading
        sound.load(); // not required I think
      } catch (error) {
        console.error(`error preloading sound`, error);
      }
    });
    soundsLoaded = true; // Execute once

};
