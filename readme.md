# Spacuuu

## What is it ?

Mobile web game.

## Status

In development


## Features

Game, Offline, Add to home-screen, sounds, vibrations, playable with game-pad, Speechsynthesis, save/load and more.

## Screen Shot


![screen-shot](documentation/game-screen.jpg)





## Minimum hardware requirement


Screen 320x320px resolution.

100MB of Memory available after the browser is open

## About the code

### Dependencies

See packages.json and also hand copied and transformed into module https://github.com/davidtheclark/tabbable


### Run locally

 npm install http-server -g
 http-server


### Build for web


requires nodejs 9+

`npm run` to see commands



### difficulty

The base difficulty is 320X320
Multyplying those resolutions equally multiplies the difficulty

difficulty gets more easy as inner Width increase

    - more space to go left and right
    - compensate by increasing the number of enemy that appear
    - increase the spawn grid

difficulty gets more easy as inner Height increases

    - more time to react to missiles
    - compensate by making the game missile go down faster
    - Ymultiplier =  1 + ((BASEMULTIPLIER *(height - BASEHEIGHT)) / BASEHEIGHT)
    for 640
        1+ (( 1 * (640-320)/320) = 2
        all speed

### design decisions

onmousedown and mouseup is used

order:
mousedown
mouseup
click

to avoid changing game state and rendering inside event (click) handler, (which has some disadventages like less performant becuase it breaks the requestAnimationFrame-Frame flow)
we save the state (pressed or not) of the mouse and keyboard, and then read that state inside the update inside the requestAnimationFrame

special care is given to left and right , you might go left and then right before you mouseup left.
Also you might press right a long time and during that time slowly draggin towards another place, which means, when the thumbs finally goes up the mouse up is fired on another part of the grid...




## Git

```
git add .
git commit -m "Initial commit"
git push -u origin master
```

## Full Build (window) Android

Note: The primary platform is the web. A web view on android might be suboptimal

Only do this once, after that see fast build

### install node, npm and cordova

https://cordova.apache.org/#getstarted
https://cordova.apache.org/docs/en/latest/guide/cli/index.html#build-the-app

### install android sdk

http://spring.io/guides/gs/android/
https://developer.android.com/studio/index.html

    set ANDROID_HOME=C:\sdk-android
    set PATH=%PATH%;%ANDROID_HOME%\tools;%ANDROID_HOME%\platform-tools

### install JAVA full JDK

not just jre but jdk
not the last version but 1.8
C:\Program Files\Java\jdk1.8.0_152

http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html
depending on version

https://confluence.atlassian.com/doc/setting-the-java_home-variable-in-windows-8895.html


    Set the JAVA_HOME environment variable to the location of your JDK installation
    Set the ANDROID_HOME environment variable to the location of your Android SDK installation
    It is also recommended that you add the Android SDK's tools, tools/bin, and platform-tools directories to your PATH


### Still there ?

set system variable ANDROID_HOME to C:\sdk-android\tools  or wherever
restart command prompt after each task

### Accept licenses

https://stackoverflow.com/questions/39760172/you-have-not-accepted-the-license-agreements-of-the-following-sdk-components#answer-43003932
https://stackoverflow.com/questions/21070268/intellij-idea-13-error-please-select-android-sdk


## Fast build


cordova build android
go to apk
start emulator in android studio
drag and drop apk to emulator
https://build.phonegap.com/ or use ^honegap build

## Debug

* cordova run adroid
* chrome://inspect/#devices
* enable debug  mode android
* usb

https://github.com/aliab/website-in-webview
https://github.com/Arva/cordova-plugin-crosswalk-webview


## Problems with the code

### No delayed loading

### History and state machine

are kept in sync manually


## License

Individual files inside the project may have other explicit license.

No commercial usage without explicit permission.

This work is licensed under a [Creative Commons Attribution-NonCommercial 4.0 International License](http://creativecommons.org/licenses/by-nc/4.0/).
