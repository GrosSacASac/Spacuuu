/*
removes temporary files
main-es-module.js is not used but main-es-module.min.js is
https://stackoverflow.com/a/42343832/3238046
*/
"use strict";

import {
    deleteFile
} from "filesac";


export default function () {

    const builtFolder = "built";
    const temporaryFiles = [
        "entry-es-module.js",
        // "entry-script.js",
        "entry-script.es5.js"
    ];

    Promise.all(
        temporaryFiles.map(function (temporaryFile) {
            return `${builtFolder}/${temporaryFile}`;
        }).map(deleteFile)
    ).then(function (resolvedValue) {
        console.log(resolvedValue);
    });


}; // end export
