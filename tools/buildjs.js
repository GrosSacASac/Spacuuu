"use strict";

import {
    textFileContent,
    writeTextInFile,
    copyFile
} from "filesac";

import rollup from "rollup";
import rollup_babel from "rollup-plugin-babel";
import terser from "terser";




export default function ({
    minification = true,
    version
}) {


    const REPLACED_WITH_SERVICE_WORKER_VERSION = "REPLACED_WITH_SERVICE_WORKER_VERSION";

    const preamble = `/*${version}*/\n`;
    const drop_console = true;


    // entry points
    const files = [
        "entry"
    ];

    async function rollupBundle(inputOptions, outputOptions) {
        // create a bundle
        const bundle = await rollup.rollup(inputOptions);

        //console.log(bundle.imports); // an array of external dependencies
        const written = await bundle.write(outputOptions);
        return written;
    }

    // first build <script nomodule src="">
    const bundlePromise = Promise.all(files.map(function (fileName) {
        /* rollups bundles and transpiles except node_modules imports
        */

        const inputOptions = {
            input: `js/${fileName}.js`,
            treeshake: {
                pureExternalModules: false,
                propertyReadSideEffects: false // assume reading properties has no side effect
            },
            plugins: [
                rollup_babel({
                    babelrc: false,
                    // exclude: 'node_modules/**',
                    externalHelpers: false,
                    "plugins": [
                    ],
                    "presets": [
                        [
                            "@babel/preset-env",
                            {
                                //"useBuiltIns": 'usage',
                                "loose": true,
                                "modules": false,
                                "targets": {
                                    "browsers": [
                                        "> 0.1%",
                                    ]
                                }
                            }
                        ]
                    ]
                })
            ]
        };
        const outputOptions = {
            format: "iife",
            name: `${fileName}`,
            file: `built/${fileName}-script.es5.js`
        };

        return rollupBundle(inputOptions, outputOptions)
    })).then(function () {
        return Promise.all(files.map(function (fileName) {
            return textFileContent(`built/${fileName}-script.es5.js`)
                .then(function (content) {
                    return [fileName, content];
                });

        })).then(function (contents) {
            return Promise.all(contents.map(function ([fileName, code]) {
                let resultCode;
                if (!minification) {
                    resultCode = code;
                } else {
                    const result = terser.minify(code, {
                        ecma: 5,
                        module: false,
                        toplevel: true,
                        ie8: false,
                        parse: {
                        },
                        mangle: {
                            toplevel: true
                        },
                        compress: {
                            passes: 4,
                            ecma: 5,
                            ie8: false,
                            drop_console,
                            keep_infinity: true,
                            keep_fargs: false,
                            module: false,
                            unsafe: false,
                            unsafe_arrows: true,
                            unsafe_methods: true,
                            unsafe_math: true,
                        },
                        output: {
                            ecma: 5,
                            beautify: false,
                            preamble
                        }
                    });
                    if (result.error) {
                        console.error(result);
                        return Promise.reject(result.error);
                    }
                    resultCode = result.code;
                }
                return writeTextInFile(
                    `built/${fileName}-script.es5.min.js`,
                    resultCode
                );
            }));
        });
    });


    // second build <script type="module" src="">
    const bundlePromise2 = Promise.all(files.map(function (fileName) {
        const inputOptions = {
            input: `js/${fileName}.js`,
            treeshake: {
                pureExternalModules: false,
                propertyReadSideEffects: false // assume reading properties has no side effect
            },
            plugins: [
                rollup_babel({
                    babelrc: false,
                    //exclude: 'node_modules/**',
                    externalHelpers: false,
                    "plugins": [
                    ],
                    "presets": [
                        [
                            "@babel/preset-env",

                            {
                                // "useBuiltIns": 'usage',
                                "loose": true,
                                "modules": false,
                                "targets": {
                                    "esmodules": true
                                }
                            }
                        ]
                    ]
                })
            ]
        };
        // https://rollupjs.org/#core-functionality
        const outputOptions = {
            format: "es",
            name: `${fileName}`,
            file: `built/${fileName}-es-module.js`
        };
        return rollupBundle(inputOptions, outputOptions);
    }));



    const pModule = bundlePromise2.then(function () {
        /* as every browser supporting <script type="module" src="">
            also support es2015 we don't need to transpile it
            only minify it*/
        return Promise.all(files.map(function (fileName) {
            return textFileContent(`built/${fileName}-es-module.js`)
                .then(function (content) {
                    return [fileName, content];
                });

        })).then(function (contents) {
            return Promise.all(contents.map(function ([fileName, code]) {
                let resultCode;
                if (!minification) {
                    resultCode = code;
                } else {
                    const result = terser.minify("'use strict';\n" + code, {
                        ecma: 8,
                        module: true,
                        toplevel: true,
                        ie8: false,
                        parse: {
                        },
                        mangle: {
                            toplevel: true
                        },
                        compress: {
                            passes: 4,
                            ecma: 6,
                            ie8: false,
                            drop_console,
                            keep_infinity: true,
                            keep_fargs: false,
                            module: true,
                            unsafe: false,
                            unsafe_arrows: true,
                            unsafe_methods: true,
                            unsafe_math: true,
                        },
                        output: {
                            ecma: 6,
                            beautify: false,
                            preamble
                        }
                    });
                    if (result.error) {
                        console.error(result);
                        return Promise.reject(result.error);
                    }
                    resultCode = result.code;
                }
                return writeTextInFile(
                    `built/${fileName}-es-module.min.js`,
                    resultCode
                );
            }));
        });
    });



    // service worker
    const pServiceWorker = textFileContent(`js/service_worker/service_worker.js`)
        .then(function (content) {

            let resultCode;
            if (!minification) {
                resultCode = content;
            } else {
                const result = terser.minify(content, {
                    ecma: 6,
                    module: false,
                    toplevel: true,
                    ie8: false,
                    parse: {
                    },
                    mangle: {
                        toplevel: true
                    },
                    compress: {
                        passes: 4,
                        ecma: 6,
                        ie8: false,
                        drop_console,
                        keep_infinity: true,
                        keep_fargs: false,
                        module: false,
                        unsafe: true,
                        unsafe_arrows: true,
                        unsafe_methods: true,
                        unsafe_math: true,
                    },
                    output: {
                        ecma: 6,
                        beautify: false,
                        preamble
                    }
                });
                if (result.error) {
                    console.log("error:", result);
                    return Promise.reject(result.error);
                }
                resultCode = result.code;
            }

            resultCode = resultCode.replace(
                REPLACED_WITH_SERVICE_WORKER_VERSION,
                `${version}${(new Date()).toJSON()}`
            );

            return writeTextInFile(
                `built/service_worker.min.js`,
                resultCode
            );
        });

    return Promise.all([bundlePromise, pModule, pServiceWorker]);

};