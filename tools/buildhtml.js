/*

*/

"use strict";

import {
    textFileContent,
    writeTextInFile,
    copyFile
} from "filesac";

import htmlMinifier from "html-minifier";


const minify = htmlMinifier.minify;

const OPTIONS = {
    removeAttributeQuotes: false,
    caseSensitive: true,
    collapseBooleanAttributes: true,
    collapseInlineTagWhitespace: false,
    collapseWhitespace: true,
    decodeEntities: true,
    html5: true,
    removeComments: true,
    removeEmptyAttributes: true,
    removeRedundantAttributes: false
};


export default function ({ minification = true }) {

    const thisName = "html build";
    const replaceMap = {
        [`<link rel="stylesheet" href="css/game.css">`]: `<link rel="stylesheet" href="game.css">`,
        [`<script type="module" src="js/entry.js">`]: `<script type="module" src="entry-es-module.min.js">`,
        [`<script nomodule src="node_modules/@babel/polyfill/dist/polyfill.min.js"></script>`]: `<script nomodule src="polyfill.min.js"></script>`,
        [`<script nomodule src="node_modules/dom99/polyfills/built/dom-hidden.js"></script>`]: `<script nomodule src="dom-hidden.js"></script>`,
        [`<script nomodule src="node_modules/dom99/polyfills/built/remove.js"></script>`]: `<script nomodule src="remove.js"></script>`,
        [`<script nomodule src="node_modules/template-mb/template.js"></script>`]: `<script nomodule src="template.js"></script>`,
        [`<script nomodule src="node_modules/dom99/polyfills/template-mb-bootstrap.js"></script>`]: `<script nomodule src="template-mb-bootstrap.js"></script>`,



        [`<script nomodule src="built/entry-script.es5.min.js"></script>`]: `<script nomodule src="entry-script.es5.min.js"></script>`
    };

    const INDEX_PATH = "index.html";
    const BUILT_PATH = "built/index.html";

    Promise.all([
        textFileContent(INDEX_PATH),
    ]).then(function ([indexHTMLString]) {
        let newIndex = indexHTMLString;
        Object.entries(replaceMap).forEach(function ([old, neW]) {
            newIndex = newIndex.replace(old, neW);
        });

        let minifiedHtml;
        if (!minification) {
            minifiedHtml = newIndex;
        } else {
            minifiedHtml = minify(newIndex, OPTIONS);
        }
        return writeTextInFile(BUILT_PATH, minifiedHtml);
    }).then(function () {
        //console.log(thisName + " finished with success !");

    }).catch(function (reason) {
        const errorText = thisName + " failed: " + String(reason);
        console.log(errorText);
        throw new Error(errorText);
    });


    const replaceMap2 = {
        [`<link rel="stylesheet" href="css/baseline.css">`]: `<link rel="stylesheet" href="baseline.css">`,
        [`<link rel="stylesheet" href="css/menus.css">`]: `<link rel="stylesheet" href="menus.css">`,
        [`<link rel="stylesheet" href="css/controls.css">`]: `<link rel="stylesheet" href="controls.css">`,
        [`<link rel="stylesheet" href="css/open_source.css">`]: `<link rel="stylesheet" href="open_source.css">`,
        [`index.html`]: `/`
    };

    const extraHTMLFiles = [
        `controls.html`,
        `credits.html`,
        `open_source.html`
    ];

    Promise.all(
        extraHTMLFiles.map(textFileContent),
    ).then(function (HTMLStrings) {
        return Promise.all(HTMLStrings.map(function (HTMLString, i) {
            let string = HTMLString;
            Object.entries(replaceMap2).forEach(function ([old, neW]) {
                string = string.replace(old, neW);
            });
            let minifiedHtml;
            if (!minification) {
                minifiedHtml = string;
            } else {
                minifiedHtml = minify(string, OPTIONS);
            }
            return writeTextInFile(`${"built"}/${extraHTMLFiles[i]}`, minifiedHtml);
        }));

    }).then(function () {
        //console.log(thisName + " finished with success !");

    }).catch(function (reason) {
        const errorText = thisName + " failed: " + String(reason);
        console.log(errorText);
        throw new Error(errorText);
    });


};