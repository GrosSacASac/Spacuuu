import {
    textFileContent,
    writeTextInFile,
    copyFile
} from "filesac";


export default function () {

    const thisName = "buildAssets.js";

    Promise.all([
        // manifest
        copyFile(`manifest.webmanifest`, `built/manifest.webmanifest`),
        // sounds
        // Error for this file only hmmm
        // copyFile(`music/ignite_to_light_by_pc_iii.mp3`, `built/music/ignite_to_light_by_pc_iii.mp3`),
        copyFile(`music/silencyde_shroud.mp3`, `built/music/silencyde_shroud.mp3`),
        copyFile(`music/sappheiros_fading.mp3`, `built/music/sappheiros_fading.mp3`),
        copyFile(`music/chan_walrus/chan_walrus_1.mpeg`, `built/music/chan_walrus/chan_walrus_1.mpeg`),
        copyFile(`music/chan_walrus/chan_walrus_2.mpeg`, `built/music/chan_walrus/chan_walrus_2.mpeg`),
        copyFile(`music/ignite_to_light_by_pc_iii.mp3`, `built/music/ignite_to_light_by_pc_iii.mp3`),
        copyFile(`sounds/bigBomb.mp3`, `built/sounds/bigBomb.mp3`),
        // images
        copyFile(`images/logo144.png`, `built/images/logo144.png`),
        copyFile(`images/logo192.png`, `built/images/logo192.png`),
        copyFile(`images/logo-optimized2.svg`, `built/images/logo-optimized2.svg`),
        copyFile(`images/boss-targeter.svg`, `built/images/boss-targeter.svg`),
        copyFile(`images/diagonale-bomb.svg`, `built/images/diagonale-bomb.svg`),
        copyFile(`images/drone-missile.svg`, `built/images/drone-missile.svg`),
        copyFile(`images/enemy-diagonale.svg`, `built/images/enemy-diagonale.svg`),
        copyFile(`images/enemy-simple.svg`, `built/images/enemy-simple.svg`),
        copyFile(`images/missile.svg`, `built/images/missile.svg`),
        copyFile(`images/missile-2.svg`, `built/images/missile-2.svg`),
        copyFile(`images/missile-3.svg`, `built/images/missile-3.svg`),
        copyFile(`images/player.svg`, `built/images/player.svg`),
        copyFile(`images/player-2.svg`, `built/images/player-2.svg`),
        copyFile(`images/rotator.svg`, `built/images/rotator.svg`),
        copyFile(`images/explosion-launcher.svg`, `built/images/explosion-launcher.svg`),
        copyFile(`images/explosions-debris.svg`, `built/images/explosions-debris.svg`),
        copyFile(`images/explosion-debris.svg`, `built/images/explosion-debris.svg`),
        copyFile(`images/explosion-capsule.svg`, `built/images/explosion-capsule.svg`),
        // scripts

        copyFile(`node_modules/@babel/polyfill/dist/polyfill.min.js`, `built/polyfill.min.js`),
        copyFile(`node_modules/dom99/polyfills/built/dom-hidden.js`, `built/dom-hidden.js`),
        copyFile(`node_modules/dom99/polyfills/built/remove.js`, `built/remove.js`),
        copyFile(`node_modules/template-mb/template.js`, `built/template.js`),
        copyFile(`node_modules/dom99/polyfills/template-mb-bootstrap.js`, `built/template-mb-bootstrap.js`),
        // docs
        copyFile(`documentation/game-grid.png`, `built/documentation/game-grid.png`),
        copyFile(`documentation/gamepad-controls.svg`, `built/documentation/gamepad-controls.svg`),


    ]).then(function () {
        console.log(thisName, "success copy files");

    });


}; // end export
