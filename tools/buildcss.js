'use strict'
import postcss from 'postcss'
import fs from "fs";
import path from "path";
import importPostCss from 'import-postcss';
import cssnano from "cssnano";
import {
    textFileContent,
    writeTextInFile,
    copyFile
} from "filesac";


export default function ({ minification = true }) {

    const inputs = [
        'css/baseline.css',
        'css/game.css',
        'css/menus.css',
        'css/open_source.css',
        'css/controls.css',
    ];
    const outputdir = 'built';

    const plugins = [importPostCss];
    if (minification) {
        plugins.push(cssnano);
    }

    const config = {
        options: {
            map: false,
            parser: undefined,
            syntax: undefined,
            stringifier: undefined
        },
        plugins
    }

    files(inputs.map(input => path.resolve(input)))

        .then(results => {

        })
        .catch(err => {
            error(err)

            process.exit(1)
        })


    function files(files) {
        if (typeof files === 'string') files = [files]

        return Promise.all(
            files.map(file => {
                return textFileContent(file).then(content => css(content, file))
            })
        )
    }

    function css(css, file) {
        const ctx = {
            options: config.options,
            file: {
                dirname: path.dirname(file),
                basename: path.basename(file),
                extname: path.extname(file)
            }
        };

        const options = {}

        const relativePath = path.relative(path.resolve(), file)
        console.info(`Processing ${relativePath}`);

        options.from = file
        options.to = path.join(outputdir, ctx.file.basename);


        return postcss(config.plugins)
            .process(css, options)
            .then(result => {


                return writeTextInFile(options.to, result.css).then(() => {


                    if (result.warnings().length) {
                        console.warn(result.warnings())
                    }

                    return result
                })
            })
    }


    function error(err) {
        console.error(err)
        process.exit(1)
    }

};
